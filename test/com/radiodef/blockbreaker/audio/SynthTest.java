package com.radiodef.blockbreaker.audio;

import static com.radiodef.blockbreaker.audio.Synth.*;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

/**
 * @author David Staver
 */
class SynthTest {
    final double epsilon = 1.0 / 1024.0;
    
    @Test
    void setNoteTest() {
        assertEquals(14080, parseFrequency("A9"), epsilon);
        assertEquals( 7040, parseFrequency("A8"), epsilon);
        assertEquals( 3520, parseFrequency("A7"), epsilon);
        assertEquals( 1760, parseFrequency("A6"), epsilon);
        assertEquals(  880, parseFrequency("A5"), epsilon);
        assertEquals(  440, parseFrequency("A4"), epsilon);
        assertEquals(  220, parseFrequency("A3"), epsilon);
        assertEquals(  110, parseFrequency("A2"), epsilon);
        assertEquals(   55, parseFrequency("A1"), epsilon);
        assertEquals( 27.5, parseFrequency("A0"), epsilon);
        
        assertEquals(880 * 1.189207, parseFrequency("C5"), epsilon);
        assertEquals(110 * 1.498307, parseFrequency("E2"), epsilon);
        
        assertEquals(880 * 1.414214, parseFrequency("Eb5"), epsilon);
        assertEquals(220 * 1.259921, parseFrequency("C#3"), epsilon);
        
        assertEquals(440, parseFrequency("A"));
        assertEquals(parseFrequency("D4"), parseFrequency("D"));
    }
}