package com.radiodef.blockbreaker.util;

import java.awt.geom.*;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

/**
 * @author David Staver
 */
class SILineTest {
    final double epsilon = 1.0 / 1024.0;
    
    @Test
    void setLineTest() {
        SILine line = new SILine();
        
        line.set(0, 0, 1, 1);
        
        assertEquals(1.0, line.m, epsilon);
        assertEquals(0.0, line.b, epsilon);
        
        line.set(0, 1, 1, 2);
        
        assertEquals(1.0, line.m, epsilon);
        assertEquals(1.0, line.b, epsilon);
        
        // not a line
        line.set(0, 0, 0, 0);
        
        assertTrue(Double.isNaN(line.m));
        
        // vertical
        line.set(3, 0, 3, 1);
        
        assertTrue(Double.isInfinite(line.m));
        assertEquals(3, line.b);
        
        // horizontal
        line.set(1, -1, 10, -1);
        
        assertEquals( 0, line.m);
        assertEquals(-1, line.b);
    }
    
    @Test
    void solveSameTest() {
        SILine a = new SILine();
        SILine b = new SILine();
        
        Point2D.Double p = new Point2D.Double();
        
        a.set(1, 0);
        b.set(1, 0);
        
        assertFalse(a.solve(b, p));
        
        a.set(+0.0, 0);
        b.set(-0.0, 0);
        
        assertFalse(a.solve(b, p));
        
        a.set(Double.POSITIVE_INFINITY, 0);
        b.set(Double.NEGATIVE_INFINITY, 0);
        
        assertFalse(a.solve(b, p));
        
        a.set(Double.NaN, Double.NaN);
        b.set(Double.NaN, Double.NaN);
        
        assertFalse(a.solve(b, p));
    }
    
    @Test
    void solveTest() {
        SILine a = new SILine();
        SILine b = new SILine();
        
        Point2D.Double p = new Point2D.Double();
        
        a.set( 1, 0);
        b.set(-1, 0);
        
        assertTrue(a.solve(b, p));
        
        assertEquals(0, p.x, epsilon);
        assertEquals(0, p.y, epsilon);
        
        // y =  x + 1
        // y = 2x + 2
        // x + 1 = 2x + 2
        // x - 2x = 2 - 1
        // -x = 1
        // x = -1
        // y = -1 + 1 = 0
        a.set(1, 1);
        b.set(2, 2);
        
        assertTrue(a.solve(b, p));
        
        assertEquals(-1, p.x, epsilon);
        assertEquals( 0, p.y, epsilon);
        
        a.set(Double.POSITIVE_INFINITY, 1);
        b.set(1, 0); // y = x
        
        assertTrue(a.solve(b, p));
        
        assertEquals(1, p.x);
        assertEquals(1, p.y);
    }
    
    @Test
    void solveCircleTest() {
        var circle = new Circle();
        var line   = new SILine();
        var p      = new Point2D.Double();
        var n      = new Point2D.Double();
        
        circle.set(0, 0, 1);
        
        line.set(1, -2);
        assertEquals(0, line.solve(circle, p, n));
        
        line.set(0, 0);
        assertEquals(2, line.solve(circle, p, n));
        
        assertEquals(1, p.x, epsilon);
        assertEquals(0, p.y, epsilon);
        
        assertEquals(-1, n.x, epsilon);
        assertEquals( 0, n.y, epsilon);
        
        line.set(Double.POSITIVE_INFINITY, 1);
        assertEquals(1, line.solve(circle, p, n));
        
        assertEquals(1, p.x, epsilon);
        assertEquals(0, p.y, epsilon);
        
        // x = y
        // x² + y² = 1²
        // 2x² = 1
        // x² = 1/2
        // x = √(1/2)
        var sqrt1_2 = Math.sqrt(0.5);
        
        line.set(1, 0);
        assertEquals(2, line.solve(circle, p, n));
        
        assertEquals(sqrt1_2, p.x, epsilon);
        assertEquals(sqrt1_2, p.y, epsilon);
        
        assertEquals(-sqrt1_2, n.x, epsilon);
        assertEquals(-sqrt1_2, n.y, epsilon);
        
        line.set(Double.POSITIVE_INFINITY, sqrt1_2);
        assertEquals(2, line.solve(circle, p, n));
        
        assertEquals(sqrt1_2, p.x, epsilon);
        assertEquals(sqrt1_2, p.y, epsilon);
        
        assertEquals( sqrt1_2, n.x, epsilon);
        assertEquals(-sqrt1_2, n.y, epsilon);
        
        // {n = 23.000000, s = 23.500000, e = 18.000000, w = 14.000000}
        // (x - 14.000000)^2 + (y - 23.500000)^2 = 0.375000^2
        circle.set(14, 23.5, 0.375);
        // x = 16.0
        line.set(Double.POSITIVE_INFINITY, 16);
        
        assertTrue(Double.isNaN(circle.getY(line.b, false)));
        assertTrue(Double.isNaN(circle.getY(line.b, true)));
        
        assertEquals(0, line.solve(circle, p, n));
    }
}