package com.radiodef.blockbreaker.util;

import java.awt.geom.*;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

/**
 * @author David Staver
 */
class CircleTest {
    @Test
    void getQuadrantTest() {
        var c = new Circle().set(0, 0, 1);
        var p = new Point2D.Double();
        
        // note: y-axis coordinates are negated because Swing
        
        p.x = 0;
        p.y = 0;
        assertEquals(Circle.Q_1 | Circle.Q_2 | Circle.Q_3 | Circle.Q_4, c.getQuadrant(p));
        
        p.x = 2;
        p.y = 0;
        assertEquals(Circle.Q_1 | Circle.Q_4, c.getQuadrant(p));
        
        p.x = -2;
        p.y = 0;
        assertEquals(Circle.Q_2 | Circle.Q_3, c.getQuadrant(p));
        
        p.x = 0;
        p.y = -2;
        assertEquals(Circle.Q_1 | Circle.Q_2, c.getQuadrant(p));
        
        p.x = 0;
        p.y = 2;
        assertEquals(Circle.Q_3 | Circle.Q_4, c.getQuadrant(p));
        
        p.x = 2;
        p.y = -2;
        assertEquals(Circle.Q_1, c.getQuadrant(p));
        
        p.x = -2;
        p.y = -2;
        assertEquals(Circle.Q_2, c.getQuadrant(p));
        
        p.x = -2;
        p.y = 2;
        assertEquals(Circle.Q_3, c.getQuadrant(p));
        
        p.x = 2;
        p.y = 2;
        assertEquals(Circle.Q_4, c.getQuadrant(p));
    }
}