package com.radiodef.blockbreaker.util;

import java.awt.geom.*;
import java.io.*;

/**
 * @author David Staver
 */
public class SILine implements Cloneable, Serializable {
    private static final long serialVersionUID = 0L;
    
    public double m;
    // note: when this is a vertical line (m is infinite),
    //       then b is used to store x in the formula x(y) = x.
    public double b;
    
    public SILine() {
    }
    
    public SILine set(Line2D line) {
        return set(line.getX1(), line.getY1(), line.getX2(), line.getY2());
    }
    
    public SILine set(Point2D p1, Point2D p2) {
        return set(p1.getX(), p1.getY(), p2.getX(), p2.getY());
    }
    
    public SILine set(double x1, double y1, double x2, double y2) {
        // y - y1 = m(x - x1)
        // m = (y - y1) / (x - x1)
        double m = (y2 - y1) / (x2 - x1);
        
        double b;
        if (Double.isInfinite(m)) {
            // see note on this.b declaration about vertical lines
            b = x1;
        } else {
            // y = mx + b
            // b = y - mx
            b = y1 - m * x1;
        }
        
        return set(m, b);
    }
    
    public SILine set(SILine that) {
        return set(that.m, that.b);
    }
    
    public SILine set(double m, double b) {
        return setM(m).setB(b);
    }
    
    public SILine setM(double m) {
        if (m == Double.NEGATIVE_INFINITY) {
            m = Double.POSITIVE_INFINITY;
        } else if (m == 0) {
            m = 0;
        }
        this.m = m;
        return this;
    }
    
    public SILine setB(double b) {
        this.b = (b == 0) ? 0 : b;
        return this;
    }
    
    public double getM() { return m; }
    public double getB() { return b; }
    
    public double getY(double x) {
        return (m * x) + b;
    }
    
    public double getX(double y) {
        // see note on this.b declaration about vertical lines
        if (isVertical())
            return b;
        // y = mx + b
        // y - b = mx
        // x = (y - b) / m
        return (y - b) / m;
    }
    
    public boolean isVertical() {
        return Double.isInfinite(m);
    }
    
    public boolean isHorizontal() {
        return m == 0;
    }
    
    public boolean isLine() {
        // note: this first check is the only one that's
        //       expected to be ever hit. (it would be
        //       hit in the case where we set the line
        //       to some Line2D where p1 == p2, e.g. a
        //       stationary object.)
        if (Double.isNaN(m))
            return false;
        // these tests are just here for the sake of
        // good behavior.
        var b = this.b;
        if (Double.isNaN(b))
            return false;
        if (Double.isInfinite(b))
            return false;
        return true;
    }
    
    // note: we don't count the lines as intersecting
    //       if they are the same line.
    public boolean solve(SILine that, Point2D result) {
        if (this.isLine() && that.isLine()) {
            if (this.m != that.m) {
                double x, y;
                
                if (this.isVertical()) {
                    if (that.isVertical()) {
                        return false;
                    }
                    // see note on this.b declaration about vertical lines
                    x = this.b;
                    y = that.getY(x);
                } else if (that.isVertical()) {
                    // see note on this.b declaration about vertical lines
                    x = that.b;
                    y = this.getY(x);
                } else {
                    // (this.m * x) + this.b = (that.m * x) + that.b
                    // (this.m * x) - (that.m * x) + this.b = that.b
                    // (this.m * x) - (that.m * x) = that.b - this.b
                    // x(this.m - that.m) = that.b - this.b
                    // x = (that.b - this.b) / (this.m - that.m)
                    x = (that.b - this.b) / (this.m - that.m);
                    y = this.getY(x);
                }
                
                assert Tools.isReal(x) && Tools.isReal(y)
                     : String.format("(%s, %s), (%s) with (%s)", x, y, this, that);
                
                result.setLocation(x, y);
                return true;
            }
        }
        return false;
    }
    
    public int solve(Circle that, Point2D pResult, Point2D nResult) {
        if (this.isLine()) {
            var m = this.m;
            var s = this.b;
            
            var a = that.a;
            var b = that.b;
            var r = that.r;
            
            if (this.isVertical()) {
                // (x - a)² + (y - b)² = r²
                // (y - b)² = r² - (x - a)²
                // y - b = ±√(r² - (x - a)²)
                // y = ±√(r² - (x - a)²) + b
                var x = s;
                
                var discriminant = (r * r) - ((x - a)*(x - a));
                if (discriminant < 0)
                    return 0;
                
                var sqrt = Math.sqrt(discriminant);
                
                pResult.setLocation(x, sqrt + b);
                if (discriminant == 0)
                    return 1;
                
                nResult.setLocation(x, -sqrt + b);
                return 2;
            }
            
            // y = mx + s
            // (x - a)² + (y - b)² = r²
            //
            // (x - a)² + ((mx + s) - b)² = r²
            // (x - a)² + ((mx + s) - b)² - r² = 0
            //
            // (x - a)²
            //  = x(x - a) - a(x - a)
            //  = x² - ax - ax + a²
            //  = x² - 2ax + a²
            //
            // ((mx + s) - b)²
            //  = (mx + s - b)²
            //  = mx(mx + s - b) + s(mx + s - b) - b(mx + s - b)
            //  = (m²x² + smx - bmx) + (smx + s² - sb) - (bmx + sb - b²)
            //  = m²x² + smx - bmx + smx + s² - sb - bmx - sb + b²
            //  = m²x² + 2smx - 2bmx - 2sb + s² + b²
            //
            // (x - a)² + ((mx + s) - b)² - r²
            //  = (x² - 2ax + a²) + (m²x² + 2smx - 2bmx - 2sb + s² + b²) - r²
            //  = x² - 2ax + a² + m²x² + 2smx - 2bmx - 2sb + s² + b² - r²
            //
            //  = (m²x² + x²) + (2smx - 2bmx - 2ax) + (a² - 2sb + s² + b² - r²)
            //  = x²(m² + 1) + x(2sm - 2bm - 2a) + (a² - 2sb + s² + b² - r²)
            //
            // (m² + 1)x² + (2sm - 2bm - 2a)x + (a² - 2sb + s² + b² - r²) = 0
            //
            // A = (m² + 1)
            // B = (2sm - 2bm - 2a)
            // C = (a² - 2sb + s² + b² - r²)
            //
            // Ax² + Bx + C = 0
            //
            //     -B ± √(B² - 4AC)
            // x = ────────────────
            //           2A
            //
            //     -(2sm - 2bm - 2a) ± √((2sm - 2bm - 2a)² - 4(m² + 1)(a² - 2sb + s² + b² - r²))
            //   = ─────────────────────────────────────────────────────────────────────────────
            //                                      2(m² + 1)
            
            var qA = (m * m) + 1;
            var qB = (2 * s * m) - (2 * b * m) - (2 * a);
            var qC = (a * a) - (2 * s * b) + (s * s) + (b * b) - (r * r);
            
            var discriminant = (qB * qB) - (4 * qA * qC);
            if (discriminant < 0)
                return 0;
            
            var sqrt = Math.sqrt(discriminant);
            
            var x1 = (-qB + sqrt) / (2 * qA);
            var y1 = getY(x1);
            
            pResult.setLocation(x1, y1);
            if (discriminant == 0)
                return 1;
            
            var x2 = (-qB - sqrt) / (2 * qA);
            var y2 = getY(x2);
            
            nResult.setLocation(x2, y2);
            return 2;
        }
        return 0;
    }
    
    @Override
    public final int hashCode() {
        return (31 * Tools.rawBitHash(m)) + Tools.rawBitHash(b);
    }
    
    @Override
    public final boolean equals(Object obj) {
        if (obj == this)
            return true;
        if (obj instanceof SILine) {
            SILine that = (SILine) obj;
            if (!Tools.rawBitsEqual(this.m, that.m))
                return false;
            if (!Tools.rawBitsEqual(this.b, that.b))
                return false;
            return true;
        }
        return false;
    }
    
    @Override
    public String toString() {
        if (isVertical()) {
            // see note on this.b declaration about vertical lines
            return "x = " + b;
        } else if (isHorizontal()) {
            return "y = " + b;
        } else {
            return "y = " + m + " * x + " + b;
        }
    }
    
    @Override
    public SILine clone() {
        try {
            return (SILine) super.clone();
        } catch (CloneNotSupportedException x) {
            throw new AssertionError(getClass().getName(), x);
        }
    }
}