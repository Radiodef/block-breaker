package com.radiodef.blockbreaker.util;

import com.radiodef.blockbreaker.app.*;

import java.awt.geom.*;
import java.io.*;

/**
 * @author David Staver
 */
// (x - a)² + (y - b)² = r²
public class Circle implements Cloneable, Serializable {
    private static final long serialVersionUID = 0L;
    
    public double a;
    public double b;
    public double r;
    
    public Circle() {
    }
    
    public Circle set(Point2D p, double r) {
        return set(p.getX(), p.getY(), r);
    }
    
    public Circle set(double a, double b, double r) {
        return setA(a).setB(b).setR(r);
    }
    
    public Circle setA(double a) {
        this.a = a;
        return this;
    }
    
    public Circle setB(double b) {
        this.b = b;
        return this;
    }
    
    public Circle setR(double r) {
        this.r = Math.abs(r);
        return this;
    }
    
    public double getY(double x, boolean negative) {
        // (x - a)² + (y - b)² = r²
        // (y - b)² = r² - (x - a)²
        // y - b = ±√(r² - (x - a)²)
        // y = ±√(r² - (x - a)²) + b
        double sq = sq(r) - sq(x - a);
        if (sq < 0) {
            return Double.NaN;
        }
        double sqrt = Math.sqrt(sq);
        return (negative ? -sqrt : sqrt) + b;
    }
    
    public double getX(double y, boolean negative) {
        var a = this.a;
        var b = this.b;
        var r = this.r;
        // (x - a)² + (y - b)² = r²
        // (x - a)² = r² - (y - b)²
        // x - a = ±√(r² - (y - b)²)
        // x = ±√(r² - (y - b)²) + a
        double sq = sq(r) - sq(y - b);
        if (sq < 0) {
            return Double.NaN;
        }
        double sqrt = Math.sqrt(sq);
        return (negative ? -sqrt : sqrt) + a;
    }
    
    private static double sq(double n) { return n * n; }
    
    public boolean contains(double x, double y) {
        return Tools.hypotSq(x - a, y - b) <= sq(r);
//        double dist = Math.hypot(x - a, y - b);
//        return dist <= r;
    }
    
    public Point2D clipToOutside(Point2D p) {
        var x = p.getX();
        var y = p.getY();
        var a = this.a;
        var b = this.b;
        var r = this.r;
        
        var count = 0;
        
        if (contains(x, y) && Tools.isReal(x, y)) {
            var adj = x - a;
            var opp = y - b;
            var hyp = Math.hypot(adj, opp);
            var cos = adj / hyp;
            var sin = opp / hyp;
            
            do {
                ++count;
                assert count < 10 : count;
                
                r = Tools.nextUp(r);
                x = a + (cos * r);
                y = b + (sin * r);
            } while (contains(x, y) && Tools.isReal(x, y));
            
            p.setLocation(x, y);
        }
        
//        Log.note("count = " + count);
        return p;
    }
    
    public static final int NOT_QUADRANT = 0;
    
    public static final int Q_1 = 1 << 0,
                            Q_2 = 1 << 1,
                            Q_3 = 1 << 2,
                            Q_4 = 1 << 3;
    
    public int getQuadrant(Point2D p) {
        var x = p.getX();
        var y = p.getY();
        var a = this.a;
        var b = this.b;
        
        var above = y < b;
        var below = y > b;
        var left  = x < a;
        var right = x > a;
        
        return above ? (right ? Q_1 : left ? Q_2 : (Q_1 | Q_2))
             : below ? (right ? Q_4 : left ? Q_3 : (Q_3 | Q_4))
             :         (right ? (Q_1 | Q_4) : left ? (Q_2 | Q_3) : (Q_1 | Q_2 | Q_3 | Q_4));
    }
    
    public static int cornerToQuadrant(int corner) {
        switch (corner) {
            case Box.NE:
                return Q_1;
            case Box.NW:
                return Q_2;
            case Box.SW:
                return Q_3;
            case Box.SE:
                return Q_4;
            default:
                return NOT_QUADRANT;
        }
    }
    
    @Override
    public final int hashCode() {
        int hash;
        hash = Tools.rawBitHash(a);
        hash = 31 * hash + Tools.rawBitHash(b);
        hash = 31 * hash + Tools.rawBitHash(r);
        return hash;
    }
    
    @Override
    public final boolean equals(Object obj) {
        if (obj == this)
            return true;
        if (obj instanceof Circle) {
            Circle that = (Circle) obj;
            if (!Tools.rawBitsEqual(this.a, that.a))
                return false;
            if (!Tools.rawBitsEqual(this.b, that.b))
                return false;
            if (!Tools.rawBitsEqual(this.r, that.r))
                return false;
            return true;
        }
        return false;
    }
    
    @Override
    public String toString() {
        return String.format("(x - %f)^2 + (y - %f)^2 = %f^2", a, b, r);
    }
    
    @Override
    public Circle clone() {
        try {
            return (Circle) super.clone();
        } catch (CloneNotSupportedException x) {
            throw new AssertionError(getClass().getName(), x);
        }
    }
}