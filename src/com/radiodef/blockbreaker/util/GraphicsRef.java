package com.radiodef.blockbreaker.util;

import java.awt.*;
import java.util.function.*;

/**
 * @author David Staver
 */
public final class GraphicsRef implements AutoCloseable, Supplier<Graphics2D> {
    private final Graphics2D g2;

    public GraphicsRef(Graphics g) {
        g2 = (Graphics2D) g.create();
    }

    @Override
    public Graphics2D get() {
        return g2;
    }

    @Override
    public void close() {
        g2.dispose();
    }
}
