package com.radiodef.blockbreaker.util;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import javax.swing.*;
import java.util.*;
import java.math.*;
import java.lang.reflect.*;

/**
 * @author David Staver
 */
public final class Tools {
    private Tools() {
    }
    
    public static final double NANOS_PER_SECOND_AS_DOUBLE = 1_000_000_000.0;
    
    public static final Stroke DASHED_STROKE =
        new BasicStroke(1.0f,
                        BasicStroke.CAP_ROUND,
                        BasicStroke.JOIN_ROUND,
                        1.0f,
                        new float[] {2.0f},
                        0.0f);
    
    public static final BigDecimal BIG_ONE_HUNDRED = BigDecimal.valueOf(100.0);
    
    private static final boolean USE_ULP = false;
    
    public static final double EPSILON = 1.0 / 1024.0;
    
    public static double nextDown(double n) {
        if (USE_ULP) {
            return Math.nextDown(n);
        } else {
            return Math.min(Math.nextDown(n), n - EPSILON);
        }
    }
    
    public static double nextUp(double n) {
        if (USE_ULP) {
            return Math.nextUp(n);
        } else {
            return Math.max(Math.nextUp(n), n + EPSILON);
        }
    }
    
    public static double dot(double x1, double y1, double x2, double y2) {
        return x1 * x2 + y1 * y2;
    }
    
    public static Point2D reflect(Point2D normal, Point2D v) {
        var normX = normal.getX();
        var normY = normal.getY();
        
        var x = v.getX();
        var y = v.getY();
        
        var normHypSq = Tools.hypotSq(normX, normY);
        
        var dot   = Tools.dot(x, y, normX, normY);
        var scale = 2 * dot / normHypSq;
        var projX = scale * normX;
        var projY = scale * normY;
        
        v.setLocation(x - projX, y - projY);
        return v;
    }
    
    public static int rawBitHash(double n) {
        return Long.hashCode(Double.doubleToRawLongBits(n));
    }
    
    public static boolean rawBitsEqual(double lhs, double rhs) {
        return Double.doubleToRawLongBits(lhs) == Double.doubleToRawLongBits(rhs);
    }
    
    public static boolean isReal(double n) {
        return !(Double.isInfinite(n) || Double.isNaN(n));
    }
    
    public static boolean isReal(double x, double y) {
        return isReal(x) && isReal(y);
    }
    
    public static boolean isReal(Point2D p) {
        return isReal(p.getX(), p.getY());
    }
    
    public static boolean isBetween(Point2D pt, Point2D a, Point2D b) {
        return isBetween(pt.getX(), pt.getY(), a.getX(), a.getY(), b.getX(), b.getY());
    }
    
    public static boolean isBetween(double pX,
                                    double pY,
                                    double aX,
                                    double aY,
                                    double bX,
                                    double bY) {
        if (aX < bX) {
            if (pX < aX || bX < pX)
                return false;
        } else {
            if (pX < bX || aX < pX)
                return false;
        }
        if (aY < bY) {
            if (pY < aY || bY < pY)
                return false;
        } else {
            if (pY < bY || aY < pY)
                return false;
        }
        return true;
    }
    
    public static double hypotSq(double a, double b) {
        return a*a + b*b;
    }
    
    public static double distanceSq(Point2D a, Point2D b) {
        return hypotSq(a.getX() - b.getX(), a.getY() - b.getY());
    }
    
    public static int gcd(int a, int b) {
        BigInteger bigA = BigInteger.valueOf(a);
        BigInteger bigB = BigInteger.valueOf(b);
        return bigA.gcd(bigB).intValueExact();
    }
    
    public static Dimension toAspectRatio(Dimension size) {
        int gcd = gcd(size.width, size.height);
        size.width /= gcd;
        size.height /= gcd;
        return size;
    }
    
    public static Dimension getAvailableScreenSize(JFrame frame) {
        Objects.requireNonNull(frame, "frame");
        GraphicsConfiguration config = frame.getGraphicsConfiguration();
        
        Dimension size = config.getBounds().getSize();
        Insets  insets = Toolkit.getDefaultToolkit().getScreenInsets(config);
        
        size.height -= insets.top  + insets.bottom;
        size.width  -= insets.left + insets.right;
        
        return size;
    }
    
    public static void requireEDT() {
        if (!SwingUtilities.isEventDispatchThread()) {
            throw new IllegalStateException(Thread.currentThread().toString());
        }
    }
    
    public static void whenShowing(JFrame frame, Runnable task) {
        Objects.requireNonNull(frame, "frame");
        Objects.requireNonNull(task,  "task");
        if (frame.isShowing()) {
            task.run();
        } else {
            onNextShow(frame, task);
        }
    }
    
    public static void onNextShow(JFrame frame, Runnable task) {
        Objects.requireNonNull(frame, "frame");
        Objects.requireNonNull(task,  "task");
        frame.addHierarchyListener(new HierarchyListener() {
            @Override
            public void hierarchyChanged(HierarchyEvent e) {
                if ((e.getChangeFlags() & HierarchyEvent.SHOWING_CHANGED) != 0) {
                    if (e.getChanged().isShowing()) {
                        frame.removeHierarchyListener(this);
                        task.run();
                    }
                }
            }
        });
    }
    
    private static final Map<Integer, String> KEYCODE_NAMES = new HashMap<>();
    static {
        for (Field field : KeyEvent.class.getFields()) {
            if (!Modifier.isStatic(field.getModifiers()))
                continue;
            if (!field.getName().startsWith("VK_"))
                continue;
            try {
                KEYCODE_NAMES.put(field.getInt(null), field.getName().substring(3));
            } catch (ReflectiveOperationException x) {
                throw new AssertionError(field.getName(), x);
            }
        }
    }
    
    public static String getKeyCodeName(Integer keyCode) {
        return KEYCODE_NAMES.getOrDefault(keyCode, "unknown");
    }

    public static StackWalker.StackFrame getCaller(int skip) {
        StackWalker w = StackWalker.getInstance();

        return w.walk(stream -> stream.skip(skip + 1).findFirst().get());
    }
}