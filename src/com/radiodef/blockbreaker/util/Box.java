package com.radiodef.blockbreaker.util;

import java.awt.*;
import java.awt.geom.*;
import java.io.*;

/**
 * @author David Staver
 */
public class Box implements Cloneable, Serializable {
    private static final long serialVersionUID = 0L;
    
    public double n;
    public double s;
    public double e;
    public double w;
    
    public static final int NOT_ID = -1;
    
    public static final int N = 0,
                            S = 1,
                            E = 2,
                            W = 3;
    
    public static final int MIN_ID = N,
                            MAX_ID = W;
    
    public static final int NE = 4,
                            NW = 5,
                            SE = 6,
                            SW = 7;
    
    public static final int MIN_CORNER_ID = NE,
                            MAX_CORNER_ID = SW;
    
    public static String toString(int id) {
        switch (id) {
            case N     : return "Box.N";
            case S     : return "Box.S";
            case E     : return "Box.E";
            case W     : return "Box.W";
            case NW    : return "Box.NW";
            case NE    : return "Box.NE";
            case SW    : return "Box.SW";
            case SE    : return "Box.SE";
            case NOT_ID: return "Box.NOT_ID";
            default    : return "Box.Unknown:" + id;
        }
    }
    
    public Box() {
    }
    
    public Box set(Dimension size) {
        return set(0, size.height, size.width, 0);
    }
    
    public Box set(Rectangle2D rect) {
        return setRect(rect.getX(), rect.getY(), rect.getWidth(), rect.getHeight());
    }
    
    public Box setRect(double x, double y, double w, double h) {
        return set(y, y + h, x + w, x);
    }
    
    public Box set(double n, double s, double e, double w) {
        this.n = n;
        this.s = s;
        this.e = e;
        this.w = w;
        return this;
    }
    
    public Rectangle2D get(Rectangle2D result) {
        if (result == null) {
            result = new Rectangle2D.Double();
        }
        var n = this.n;
        var s = this.s;
        var e = this.e;
        var w = this.w;
        result.setRect(w, n, e - w, s - n);
        return result;
    }
    
    public Line2D getLine(int id, Line2D result, double outset) {
        if (result == null) {
            result = new Line2D.Double();
        }
        
        var n = this.n;
        var s = this.s;
        var e = this.e;
        var w = this.w;
        
        switch (id) {
        case N:
            n -= outset;
            result.setLine(w, n, e, n);
            break;
        case S:
            s += outset;
            result.setLine(w, s, e, s);
            break;
        case E:
            e += outset;
            result.setLine(e, n, e, s);
            break;
        case W:
            w -= outset;
            result.setLine(w, n, w, s);
            break;
        default:
            assert false : id;
        }
        
        return result;
    }
    
    public Point2D getCorner(int id, Point2D result) {
        switch (id) {
            case NE: return getCorner(N, E, result);
            case NW: return getCorner(N, W, result);
            case SE: return getCorner(S, E, result);
            case SW: return getCorner(S, W, result);
            default:
                throw new IllegalArgumentException(toString(id));
        }
    }
    
    public Point2D getCorner(int yID, int xID, Point2D result) {
        if (result == null) {
            result = new Point2D.Double();
        }
        
        double x, y;
        
        switch (xID) {
            case W:
                x = w;
                break;
            case E:
                x = e;
                break;
            default:
                throw new IllegalArgumentException(toString(xID));
        }
        switch (yID) {
            case N:
                y = n;
                break;
            case S:
                y = s;
                break;
            default:
                throw new IllegalArgumentException(toString(yID));
        }
        
        result.setLocation(x, y);
        return result;
    }
    
    public boolean contains(int id, Point2D p) {
        return contains(id, p.getX(), p.getY());
    }
    
    public boolean contains(int id, double x, double y) {
        switch (id) {
            case N:
            case S:
                return w <= x && x <= e;
            case E:
            case W:
                return n <= y && y <= s;
            default:
                return false;
        }
    }
    
    public Point2D clipToInside(Point2D p, double outset, boolean includeSouth) {
        var n = this.n - outset;
        var s = this.s + outset;
        var e = this.e + outset;
        var w = this.w - outset;
        
        var x = p.getX();
        var y = p.getY();
        
        if (x <= w) x = Tools.nextUp(w);
        if (x >= e) x = Tools.nextDown(e);
        if (y <= n) y = Tools.nextUp(n);
        if (includeSouth) {
            if (y >= s) y = Tools.nextDown(s);
        }
        
        p.setLocation(x, y);
        return p;
    }
    
    public Point2D clipToOutside(Point2D p, double outset) {
        var x = p.getX();
        var y = p.getY();
        
        if (contains(x, y, outset)) {
            var nearest = getNearestEdge(x, y, outset);
            return clipToOutside(nearest, p, outset);
        }
        
        return p;
    }
    
    public Point2D clipToOutside(int id, Point2D p, double outset) {
        var x = p.getX();
        var y = p.getY();
        switch (id) {
            case N:
                var n = this.n - outset;
                if (y >= n) {
                    y = Tools.nextDown(n);
                }
                break;
            case S:
                var s = this.s + outset;
                if (y <= s) {
                    y = Tools.nextUp(s);
                }
                break;
            case E:
                var e = this.e + outset;
                if (x >= e) {
                    x = Tools.nextUp(e);
                }
                break;
            case W:
                var w = this.w - outset;
                if (x <= w) {
                    x = Tools.nextDown(w);
                }
                break;
        }
        p.setLocation(x, y);
        return p;
    }
    
    public boolean contains(Point2D p) {
        return contains(p, 0);
    }
    
    public boolean contains(Point2D p, double outset) {
        return contains(p.getX(), p.getY(), outset);
    }
    
    public boolean contains(double x, double y, double outset) {
        var n = this.n - outset;
        var s = this.s + outset;
        var e = this.e + outset;
        var w = this.w - outset;
        
        var isInside = (w <= x && x <= e)
                    && (n <= y && y <= s);
        return isInside;
    }
    
    public int getNearestEdge(double x, double y, double outset) {
        var n = this.n - outset;
        var s = this.s + outset;
        var e = this.e + outset;
        var w = this.w - outset;
        
        var distN = Math.abs(n - y);
        var distS = Math.abs(s - y);
        var distE = Math.abs(e - x);
        var distW = Math.abs(w - x);
        
        var minEdge = NOT_ID;
        var minDist = Double.MAX_VALUE;
        
        for (int id = MIN_ID; id <= MAX_ID; ++id) {
            var dist = Double.MAX_VALUE;
            switch (id) {
                case N: dist = distN; break;
                case S: dist = distS; break;
                case E: dist = distE; break;
                case W: dist = distW; break;
            }
            if (dist < minDist) {
                minEdge = id;
                minDist = dist;
            }
        }
        
        return minEdge;
    }
    
    @Override
    public final int hashCode() {
        int hash;
        hash = Tools.rawBitHash(n);
        hash = 31 * hash + Tools.rawBitHash(s);
        hash = 31 * hash + Tools.rawBitHash(e);
        hash = 31 * hash + Tools.rawBitHash(w);
        return hash;
    }
    
    @Override
    public final boolean equals(Object obj) {
        if (obj == this)
            return true;
        if (obj instanceof Box) {
            Box that = (Box) obj;
            if (!Tools.rawBitsEqual(this.n, that.n))
                return false;
            if (!Tools.rawBitsEqual(this.s, that.s))
                return false;
            if (!Tools.rawBitsEqual(this.e, that.e))
                return false;
            if (!Tools.rawBitsEqual(this.w, that.w))
                return false;
            return true;
        }
        return false;
    }
    
    @Override
    public String toString() {
        return String.format("{n = %f, s = %f, e = %f, w = %f}", n, s, e, w);
    }
    
    @Override
    public Box clone() {
        try {
            return (Box) super.clone();
        } catch (CloneNotSupportedException x) {
            throw new AssertionError(getClass().getName(), x);
        }
    }
}