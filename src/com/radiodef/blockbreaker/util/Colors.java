package com.radiodef.blockbreaker.util;

import java.awt.*;

/**
 * @author David Staver
 */
public class Colors extends Color {
    private Colors(int x) {
        super(x);
    }
    
    public static final Color TRANS_LIGHT_GRAY      = setAlpha(LIGHT_GRAY, 64);
    public static final Color VERY_TRANS_LIGHT_GRAY = setAlpha(LIGHT_GRAY, 32);
    public static final Color TRANS_BLACK           = setAlpha(BLACK,      128);
    public static final Color TRANS_RED             = setAlpha(RED,        64);
    public static final Color VERY_TRANS_RED        = setAlpha(RED,        32);
    public static final Color PURPLE                = new Color(0x800080);

    public static Color setAlpha(Color color, int alpha) {
        int rgb = color.getRGB() & 0xFF_FF_FF;
        return new Color((alpha << 24) | rgb, true);
    }
}
