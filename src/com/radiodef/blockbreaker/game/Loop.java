package com.radiodef.blockbreaker.game;

import com.radiodef.blockbreaker.util.*;

import java.util.*;
import java.awt.event.*;
import javax.swing.Timer;

/**
 * @author David Staver
 */
final class Loop {
    private static final int FPS = 30;
    
    private final Game game;
    
    private final Timer timer;
    private final Body body;
    
    Loop(Game game) {
        Tools.requireEDT();
        this.game = Objects.requireNonNull(game, "game");
        
        body  = new Body();
        timer = new Timer(1_000 / FPS, body);
        timer.setRepeats(true);
        timer.setCoalesce(false);
    }
    
    void restart() {
        stop();
        start();
    }
    
    void start() {
        Tools.requireEDT();
        if (!timer.isRunning()) {
            body.reset();
            timer.start();
        }
    }
    
    void stop() {
        Tools.requireEDT();
        if (timer.isRunning()) {
            timer.stop();
        }
    }
    
    boolean isFirstFrame() {
        Tools.requireEDT();
        return body.frameCount <= 0;
    }
    
    int getFrameCount() {
        Tools.requireEDT();
        return Math.max(0, body.frameCount);
    }
    
    long getElapsedNanos() {
        Tools.requireEDT();
        return body.elapsedNanos;
    }
    
    double getElapsedSeconds() {
        Tools.requireEDT();
        return body.elapsedSeconds;
    }
    
    private final class Body implements ActionListener {
        private int    frameCount;
        private long   lastFrameNanos;
        private long   elapsedNanos;
        private double elapsedSeconds;
        
        void reset() {
            frameCount     = -1;
            elapsedNanos   = 0;
            elapsedSeconds = 0;
        }
        
        private int nextFrameCount() {
            int frameCount = this.frameCount;
            
            if (frameCount != Integer.MAX_VALUE) {
                this.frameCount = ++frameCount;
            }
            
            return frameCount;
        }
        
        @Override
        public void actionPerformed(ActionEvent e) {
            long thisFrameNanos = System.nanoTime();
            long elapsedNanos   = 0;
            
            processControls();
            var game  = Loop.this.game;
            var state = game.getState();
            
            if (state == State.PLAY) {
                int frameCount = nextFrameCount();
                if (frameCount > 0) {
                    elapsedNanos = thisFrameNanos - this.lastFrameNanos;
                }
            }
            
            double elapsedSeconds = elapsedNanos / Tools.NANOS_PER_SECOND_AS_DOUBLE;
            
            this.elapsedNanos   = elapsedNanos;
            this.elapsedSeconds = elapsedSeconds;
            
            game.getScene().update(game, elapsedSeconds);
            game.getWindow().getPanel().repaint();
            
            this.lastFrameNanos = thisFrameNanos;
        }
        
        private void processControls() {
            var game     = Loop.this.game;
            var scene    = game.getScene();
            var controls = game.getControls();
            controls.markFrame();
            
            // Paddle movement is done with polling because it's easier
            // to get good behavior around things such as unpausing while
            // holding a movement key down.
            if (game.getState() == State.PLAY) {
                scene.setPaddleVelocity(controls.anyDown(Controls.LEFT_KEYS),
                                        controls.anyDown(Controls.RIGHT_KEYS));
            }
        }
    }
}
