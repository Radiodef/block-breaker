package com.radiodef.blockbreaker.game;

import com.radiodef.blockbreaker.main.*;
import com.radiodef.blockbreaker.util.*;

import java.awt.*;
import java.awt.geom.*;

/**
 * @author David Staver
 */
public class DebugCrossEntity extends AbstractEntity<DebugCrossEntity> {
    private final Line2D.Double line = new Line2D.Double();
    
    private final Rectangle2D.Double sceneBounds = new Rectangle2D.Double();
    
    DebugCrossEntity() {
        setVisible(BlockBreaker.isDebug());
    }
    
    @Override
    protected DebugCrossEntity self() {
        return this;
    }
    
    @Override
    public void update(Game game, double t) {
    }
    
    @Override
    protected void drawImpl(Game game, Graphics2D g2) {
        var scene       = game.getScene();
        var line        = this.line;
        var sceneBounds = this.sceneBounds;
        
        scene.getLastBoundsOnPanel(sceneBounds);
        g2.setColor(Colors.TRANS_LIGHT_GRAY);
        
        line.x1 = 0;
        line.x2 = sceneBounds.width;
        line.y1 = line.y2 = sceneBounds.height / 2.0;
        g2.draw(line);
        
        line.x1 = line.x2 = sceneBounds.width / 2.0;
        line.y1 = 0;
        line.y2 = sceneBounds.height;
        g2.draw(line);
    }
}
