package com.radiodef.blockbreaker.game;

/**
 * @author David Staver
 */
public interface GameObject {
    void update(Game game, double elapsed);
}
