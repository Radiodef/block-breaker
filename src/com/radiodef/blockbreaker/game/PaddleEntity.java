package com.radiodef.blockbreaker.game;

import com.radiodef.blockbreaker.util.*;

import java.awt.*;
import java.awt.geom.*;

/**
 * @author David Staver
 */
public class PaddleEntity
extends    AbstractEntity<PaddleEntity>
implements ColoredVisual<PaddleEntity> {
    private final Rectangle2D.Double rect      = new Rectangle2D.Double();
    private final Dimension          sceneSize = new Dimension();
    
    private final DebugHitBoxVisual hitBox = new DebugHitBoxVisual(this);
    
    private Color color;
    
    private double speed;
    
    PaddleEntity() {
        setColor(null);
        
        speed = 30;
        
        bounds.width  = 4;
        bounds.height = 0.5;
    }
    
    @Override
    protected PaddleEntity self() {
        return this;
    }
    
    @Override
    public Color getColor() {
        return color;
    }
    
    @Override
    public PaddleEntity setColor(Color color) {
        if (color == null) {
            color = Color.LIGHT_GRAY;
        }
        this.color = color;
        return this;
    }
    
    public PaddleEntity setVelocity(boolean left, boolean right) {
        if (left == right) {
            setVelocity(0, 0);
        } else {
            setVelocity(left ? -speed : +speed, 0);
        }
        return this;
    }
    
    public PaddleEntity reset(Scene scene) {
        setVelocity(false, false);
        
        var sceneSize = scene.getSizeInGameUnits(this.sceneSize);
        
        var x = sceneSize.width / 2.0;
        var y = sceneSize.height - 1.5 * bounds.height;
//        var y = sceneSize.height * 0.75;
        setLocation(x, y);
        
        return this;
    }
    
    @Override
    public Box getBox(Box result) {
        if (result == null) {
            result = new Box();
        }
        
        Rectangle2D.Double bounds = this.bounds;
        
        var x = bounds.x;
        var y = bounds.y;
        var w = bounds.width;
        var h = bounds.height;
        
        result.setRect(x - w / 2, y - h / 2, w, h);
        
        return result;
    }
    
    @Override
    public void update(Game game, double t) {
        var x = bounds.x + t * velocity.x;
        
        var sceneSize = game.getScene().getSizeInGameUnits(this.sceneSize);
        var margin    = /*bounds.height +*/ bounds.width / 2.0;
        var min       = margin;
        var max       = sceneSize.width - margin;
        
        if (x < min) x = min;
        if (x > max) x = max;
        
        bounds.x = x;
    }
    
    @Override
    protected void drawImpl(Game game, Graphics2D g) {
        var scene  = game.getScene();
        var rect   = this.rect;
        var bounds = this.bounds;
        
        rect.setRect(scene.xGuToPx(bounds.x - bounds.width / 2),
                     scene.yGuToPx(bounds.y - bounds.height / 2),
                     scene.xGuToPx(bounds.width),
                     scene.yGuToPx(bounds.height));
        
        g.setColor(getColor());
        g.fill(rect);
        g.setColor(Color.BLACK);
        g.draw(rect);
        
        hitBox.draw(game, g);
    }
}