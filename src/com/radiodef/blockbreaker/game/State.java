package com.radiodef.blockbreaker.game;

/**
 * @author David Staver
 */
public enum State {
    MENU,
    PAUSE,
    PLAY;
}
