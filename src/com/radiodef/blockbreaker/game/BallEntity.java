package com.radiodef.blockbreaker.game;

import com.radiodef.blockbreaker.main.*;
import com.radiodef.blockbreaker.util.*;
import com.radiodef.blockbreaker.app.*;
import com.radiodef.blockbreaker.audio.*;

import java.awt.*;
import java.awt.geom.*;
import java.util.*;

/**
 * @author David Staver
 */
public class BallEntity
extends    AbstractEntity<BallEntity>
implements ColoredVisual<BallEntity> {
    // temp stuff
    private final Rectangle2D.Double paddleBounds = new Rectangle2D.Double();
    private final Line2D.Double      debugVector  = new Line2D.Double();
    private final Point2D.Double     intersection = new Point2D.Double();
    private final Point2D.Double     reflection   = new Point2D.Double();
    private final Point2D.Double     oldLoc       = new Point2D.Double();
    private final Point2D.Double     newLoc       = new Point2D.Double();
    private final Ellipse2D.Double   shape        = new Ellipse2D.Double();
    private final Box                clipBox      = new Box();
    private final Circle             clipCircle   = new Circle();
    private final Point2D.Double     clipPoint    = new Point2D.Double();
    
    // debug stuff
    private boolean showVelocityVector;
    private boolean showCollisions;
    private boolean bounceOffSouthWall;
    
    private static final int MAX_COLLISIONS_SHOWN = 100;
    
    private final Deque<Point2D.Double> collisions = new ArrayDeque<>();
    
    // regular stuff
    private boolean isStationary;
    
    private Color color;
    
    private double launchOffset;
    private double launchSpeed;
    
    private final Collider[] colliders = {
        new SceneBoxCollider(),
        new PaddleBoxCollider(),
        new PaddleCornerCollider(),
        new BlockCollider(),
    };
    
    private static final int MAX_COLLISIONS_PER_FRAME = 100;
    
    private static final Sound BOUNCE_SOUND;
    static {
        Synth synth =
            new Synth().setKind(Synth.Kind.SIN)
                       .setAmplitude(0.5)
                       .setFade(0.25);
        double len = 0.25;

        BOUNCE_SOUND =
            Sound.createRandom(synth.setNotes("C5", "C6", "G6").getSound(len),
                               synth.setNotes("G5", "G6", "D6").getSound(len),
                               synth.setNotes("C6", "C7", "G7").getSound(len));
    }
    
    BallEntity() {
        setColor(null);
        
        setDiameter(0.75);
        
        launchOffset = 3.0;
        launchSpeed  = 16.0;
        
        isStationary = true;
        
        showVelocityVector = BlockBreaker.isDebug();
        showCollisions     = BlockBreaker.isDebug();
        bounceOffSouthWall = BlockBreaker.isDebug();
    }
    
    @Override
    protected BallEntity self() {
        return this;
    }
    
    public boolean isStationary() {
        return isStationary;
    }
    
    public BallEntity setStationary(boolean isStationary) {
        this.isStationary = isStationary;
        return this;
    }
    
    public BallEntity toggleVelocityVector() {
        showVelocityVector = !showVelocityVector;
        return this;
    }
    
    public BallEntity toggleBounceOffSouthWall() {
        bounceOffSouthWall = !bounceOffSouthWall;
        return this;
    }
    
    public BallEntity toggleShowCollisions() {
        showCollisions = !showCollisions;
        if (!showCollisions) {
            collisions.clear();
        }
        return this;
    }
    
    @Override
    public Color getColor() {
        return color;
    }
    
    @Override
    public BallEntity setColor(Color color) {
        if (color == null) {
            color = Color.LIGHT_GRAY;
        }
        this.color = color;
        return this;
    }
    
    public double getRadius() {
        return getDiameter() / 2.0;
    }
    
    public double getDiameter() {
        var bounds = this.bounds;
        var width  = bounds.width;
        var height = bounds.height;
        if (width == height)
            return width;
        return (width + height) / 2.0;
    }
    
    public BallEntity setRadius(double radius) {
        return setDiameter(2.0 * radius);
    }
    
    public BallEntity setDiameter(double diameter) {
        var bounds = this.bounds;
        bounds.width = bounds.height = diameter;
        return this;
    }
    
    public void reset(Scene scene) {
        followPaddleLocation(scene);
        setStationary(true);
        setVelocityToLaunchSpeed();
    }
    
    public void launch() {
        setStationary(false);
    }
    
    public void setVelocityToLaunchSpeed() {
        setVelocity(0, launchSpeed);
    }
    
    public void followPaddleLocation(Scene scene) {
        var bounds = this.bounds;
        var paddle = scene.getPaddleBounds(paddleBounds);
        
        bounds.x = paddle.getX();
        bounds.y = paddle.getY()
                 - (paddle.getHeight() / 2.0)
                 - launchOffset
                 + (bounds.height / 2.0);
    }
    
    @Override
    public void update(Game game, double t) {
        var scene = game.getScene();
        
        if (scene.isInLaunchMode()) {
            followPaddleLocation(scene);
            
        } else if (!isStationary) {
            var velocity = this.velocity;
            var bounds   = this.bounds;
            var oldLoc   = this.oldLoc;
            var newLoc   = this.newLoc;
            
            oldLoc.y = bounds.y;
            oldLoc.x = bounds.x;
            
            var distanceY = t * velocity.y;
            var distanceX = t * velocity.x;
            
            newLoc.y = oldLoc.y + distanceY;
            newLoc.x = oldLoc.x + distanceX;
            
            if (processCollisions(scene, oldLoc, newLoc) > 0) {
                BOUNCE_SOUND.play(game.getAudioPlayer());
            }
            
            clipAll(scene, newLoc);
            
            bounds.y = newLoc.y;
            bounds.x = newLoc.x;
        }
    }
    
    private int processCollisions(Scene scene, Point2D.Double oldLoc, Point2D.Double newLoc) {
        var intersection = this.intersection;
        var reflection   = this.reflection;
        var velocity     = this.velocity;
        
        reflection.setLocation(velocity);
        
        var count = 0;
        
        while (collide(scene, oldLoc, newLoc, intersection, reflection)) {
            velocity.setLocation(reflection);
            
            var totalY = newLoc.y - oldLoc.y;
            var totalX = newLoc.x - oldLoc.x;
            var difY   = intersection.y - oldLoc.y;
            var difX   = intersection.x - oldLoc.x;
            
            var totalD  = Math.hypot(totalY, totalX);
            var difD    = Math.hypot(difY, difX);
            var remainD = totalD - difD;
            
            var hyp = Math.hypot(velocity.y, velocity.x);
            var sin = velocity.y / hyp; // sin = opp/hyp
            var cos = velocity.x / hyp; // cos = adj/hyp
            
            clipAll(scene, intersection);
            oldLoc.setLocation(intersection);
            putCollision(intersection);
            
            newLoc.x = oldLoc.x + cos * remainD;
            newLoc.y = oldLoc.y + sin * remainD;
            
            if (remainD <= 0)
                break;
            if (( ++count ) == MAX_COLLISIONS_PER_FRAME) {
                Log.note("hit collision max at " + count);
                break;
            }
        }
        
        return count;
    }
    
    private void clipAll(Scene scene, Point2D p) {
        var box    = clipBox;
        var circle = clipCircle;
        var point  = clipPoint;
        var radius = getRadius();
        scene.getPaddleBox(box).clipToOutside(p, radius);
        for (var i = Box.MIN_CORNER_ID; i <= Box.MAX_CORNER_ID; ++i) {
            box.getCorner(i, point);
            circle.set(point, radius).clipToOutside(p);
        }
        scene.getBox(box).clipToInside(p, -radius, bounceOffSouthWall);
    }
    
    private void putCollision(Point2D.Double p) {
        if (showCollisions) {
            Point2D.Double q;
            if (collisions.size() < MAX_COLLISIONS_SHOWN) {
                q = new Point2D.Double();
            } else {
                q = collisions.removeFirst();
            }
            q.setLocation(p);
            collisions.addLast(q);
        }
    }
    
    private boolean collide(Scene          scene,
                            Point2D.Double oldLoc,
                            Point2D.Double newLoc,
                            Point2D.Double inters,
                            Point2D.Double reflect) {
        var minX    = Double.MAX_VALUE;
        var minY    = Double.MAX_VALUE;
        var minDist = Double.MAX_VALUE;
        var inVelX  = reflect.x;
        var inVelY  = reflect.y;
        var outVelX = inVelX;
        var outVelY = inVelY;
        
        Collider nearest = null;
        
        for (Collider c : colliders) {
            reflect.x = inVelX;
            reflect.y = inVelY;
            
            if (c.collide(scene, oldLoc, newLoc, inters, reflect)) {
                var x    = inters.x;
                var y    = inters.y;
                var dist = Tools.hypotSq(x, y);
                
                if (dist < minDist) {
                    nearest = c;
                    minX    = x;
                    minY    = y;
                    minDist = dist;
                    outVelX = reflect.x;
                    outVelY = reflect.y;
                }
            }
        }
        
        if (nearest != null) {
            nearest.chosen();
            inters.setLocation(minX, minY);
            reflect.setLocation(outVelX, outVelY);
            return true;
        }
        
        return false;
    }
    
    private interface Collider {
        boolean collide(Scene          sceneIn,
                        Point2D.Double oldLocationIn,
                        Point2D.Double newLocationIn,
                        Point2D.Double intersectionOut,
                        Point2D.Double reflectionOut);
        default void chosen() {
        }
    }
    
    private abstract class BoxCollider implements Collider {
        protected final Box           box     = new Box();
        protected final Line2D.Double boxLine = new Line2D.Double();
        protected final SILine        ballSI  = new SILine();
        protected final SILine        edgeSI  = new SILine();
        
        protected abstract boolean isInside();
        protected abstract boolean includeSouthEdge();
        
        protected abstract Box getBox(Scene scene, Box box);
        
        @Override
        public boolean collide(Scene          scene,
                               Point2D.Double oldLoc,
                               Point2D.Double newLoc,
                               Point2D.Double inters,
                               Point2D.Double reflect) {
            var box    = getBox(scene, this.box);
            var outset = isInside() ? -getRadius() : getRadius();
            var id     = collideWithBox(box, outset, includeSouthEdge(), oldLoc, newLoc, inters);
            if (id != Box.NOT_ID) {
                Log.note("collision with " + Box.toString(id));
                if (isInside()) {
                    box.clipToInside(inters, outset, includeSouthEdge());
                } else {
                    box.clipToOutside(inters, outset);
                }
                assert isInside() == box.contains(inters, outset);
                reflect(scene, id, inters, reflect);
                return true;
            }
            return false;
        }
        
        protected int collideWithBox(Box     box,
                                     double  outset,
                                     boolean includeSouthEdge,
                                     Point2D.Double oldLoc,
                                     Point2D.Double newLoc,
                                     Point2D.Double inters) {
            var line   = this.boxLine;
            var ballSI = this.ballSI;
            var edgeSI = this.edgeSI;
            
            ballSI.set(oldLoc, newLoc);
            
            var collisionID   = Box.NOT_ID;
            var collisionX    = Double.NaN;
            var collisionY    = Double.NaN;
            var collisionDist = Double.MAX_VALUE;
            
            for (int id = Box.MIN_ID; id <= Box.MAX_ID; ++id) {
                if (!includeSouthEdge && (id == Box.S))
                    continue;
                
                box.getLine(id, line, outset);
                edgeSI.set(line);
                
                if (ballSI.solve(edgeSI, inters)) {
                    if (Tools.isBetween(inters, oldLoc, newLoc)) {
                        if (box.contains(id, inters)) {
                            var x    = inters.x;
                            var y    = inters.y;
                            var dist = Tools.hypotSq(x, y);
                            if (dist < collisionDist) {
                                collisionID   = id;
                                collisionX    = x;
                                collisionY    = y;
                                collisionDist = dist;
                            }
                        }
                    }
                }
            }
            
            inters.setLocation(collisionX, collisionY);
            return collisionID;
        }
        
        protected void reflect(Scene scene, int id, Point2D.Double inters, Point2D.Double reflect) {
            switch (id) {
                case Box.N:
                case Box.S:
                    reflect.y = -reflect.y;
                    break;
                case Box.E:
                case Box.W:
                    reflect.x = -reflect.x;
                    break;
            }
        }
    }
    
    private final class SceneBoxCollider extends BoxCollider {
        @Override
        protected boolean isInside() {
            return true;
        }
        @Override
        protected boolean includeSouthEdge() {
            return bounceOffSouthWall;
        }
        @Override
        protected Box getBox(Scene scene, Box box) {
            return scene.getBox(box);
        }
    }
    
    private final class PaddleBoxCollider extends BoxCollider {
        private final Rectangle2D.Double paddle = new Rectangle2D.Double();
        private final Point2D.Double     normal = new Point2D.Double();
        
        private static final double MAX_TILT = Math.PI / 8.0;
        
        private static final double SPEED_MULTIPLIER = 0.0625;
        
        private static final double MIN_ASCENT = Math.PI / 16.0;
        private static final double MAX_ASCENT = Math.PI - MIN_ASCENT;
        
        private static final double MIN_ASCENT_COS =  0.9807852804032304;  // Math.cos(MIN_ASCENT);
        private static final double MIN_ASCENT_SIN =  0.19509032201612825; // Math.sin(MIN_ASCENT);
        private static final double MAX_ASCENT_COS = -0.9807852804032304;  // Math.cos(MAX_ASCENT);
        private static final double MAX_ASCENT_SIN =  0.1950903220161286;  // Math.sin(MAX_ASCENT);
        
        @Override
        protected void reflect(Scene scene, int id, Point2D.Double inters, Point2D.Double reflect) {
            if (id != Box.N) {
                super.reflect(scene, id, inters, reflect);
                return;
            }
            
            var paddle = this.paddle;
            scene.getPaddleBounds(paddle);
            
            var width   = paddle.width;
            var halfW   = width / 2.0;
            var difX    = inters.x - paddle.x;
            var ratio   = difX / halfW;
            
            var theta  = (Math.PI / 2.0) - (MAX_TILT * ratio);
            var normal = this.normal;
            normal.x   =  Math.cos(theta);
            normal.y   = -Math.sin(theta); // note: Swing
            
            var inVelX = reflect.x;
            
            Tools.reflect(normal, reflect);
            
            var reflectX = reflect.x;
            var reflectY = reflect.y;
            
            // note:
            //  maybe a better way to do a speed multiplier
            //  is to compute the angle between the velocity
            //  vector and normal, and the more obtuse the
            //  angle is the bigger the multiplier?
            
            var multiplier = 1.0 + (ratio * SPEED_MULTIPLIER);
            reflectX *= multiplier;
            reflectY *= multiplier;
            
            // this is a fix for the below assertion failure
            
            var ascent = Math.atan2(-reflectY, reflectX);
            if (ascent < MIN_ASCENT || MAX_ASCENT < ascent) {
                var hyp = Math.hypot(reflectY, reflectX);
                // note:
                //  if the ball's angle of decent towards the paddle
                //  was shallower than the min/max, then maybe we
                //  should allow the ascent to also be shallower than
                //  the min/max in that case.
                var cos = (inVelX < 0) ? MAX_ASCENT_COS : MIN_ASCENT_COS;
                var sin = (inVelX < 0) ? MAX_ASCENT_SIN : MIN_ASCENT_SIN;
                
                reflectX = hyp *  cos;
                reflectY = hyp * -sin;
            }
            
            reflect.setLocation(reflectX, reflectY);
            
            // this assertion was hit in a situation like the following:
            // ball is moving at a very shallow angle and hits the paddle
            // on the opposite side from its source location.
            //
            //                     B (ball starts e.g. here and
            //  _x________               collides at the 'x')
            // |          |
            assert reflectY <= 0 : reflect;
        }
        
        @Override
        protected boolean isInside() {
            return false;
        }
        @Override
        protected boolean includeSouthEdge() {
            return true;
        }
        @Override
        protected Box getBox(Scene scene, Box box) {
            return scene.getPaddleBox(box);
        }
    }
    
    private abstract class BoxCornerCollider implements Collider {
        private final Point2D.Double pResult  = new Point2D.Double();
        private final Point2D.Double nResult  = new Point2D.Double();
        private final Point2D.Double normal   = new Point2D.Double();
        private final SILine         line     = new SILine();
        private final Circle         circle   = new Circle();
        private final Point2D.Double corner   = new Point2D.Double();
        private final Box            box      = new Box();
        private final Box            sceneBox = new Box();
        
        protected abstract Box getBox(Scene scene, Box box);
        
        @Override
        public boolean collide(Scene          scene,
                               Point2D.Double oldLoc,
                               Point2D.Double newLoc,
                               Point2D.Double inters,
                               Point2D.Double reflect) {
            var pResult  = this.pResult;
            var nResult  = this.nResult;
            var line     = this.line.set(oldLoc, newLoc);
            var circle   = this.circle;
            var box      = getBox(scene, this.box);
            var sBox     = scene.getBox(this.sceneBox);
            var radius   = getRadius();
            
            var minID   = Box.NOT_ID;
            var minX    = Double.MAX_VALUE;
            var minY    = Double.MAX_VALUE;
            var minDist = Double.MAX_VALUE;
            
            for (int id = Box.MIN_CORNER_ID; id <= Box.MAX_CORNER_ID; ++id) {
                getCorner(box, id, circle, radius);
                
                Point2D.Double result;
                double dist;
                
                var count = line.solve(circle, pResult, nResult);
                switch (count) {
                case 0:
                    continue;
                case 1:
                    if (!(Tools.isBetween(pResult, oldLoc, newLoc) && sBox.contains(pResult)))
                        continue;
                    result = pResult;
                    dist   = Tools.distanceSq(oldLoc, pResult);
                    break;
                case 2:
                    var pIsValid = Tools.isBetween(pResult, oldLoc, newLoc) && sBox.contains(pResult);
                    var nIsValid = Tools.isBetween(nResult, oldLoc, newLoc) && sBox.contains(nResult);
                    if (!pIsValid && !nIsValid)
                        continue;
                    var pDist = Tools.distanceSq(oldLoc, pResult);
                    var nDist = Tools.distanceSq(oldLoc, nResult);
                    if (pIsValid && (!nIsValid || (pDist < nDist))) {
                        result = pResult;
                        dist   = pDist;
                    } else {
                        result = nResult;
                        dist   = nDist;
                    }
                    break;
                default:
                    throw new AssertionError(count);
                }
                
                if ((minID != Box.NOT_ID) && (minDist <= dist))
                    continue;
                if ((circle.getQuadrant(result) & Circle.cornerToQuadrant(id)) == 0)
                    continue;
                
                minID   = id;
                minX    = result.x;
                minY    = result.y;
                minDist = dist;
            }
            
            if (minID == Box.NOT_ID)
                return false;
            
            inters.setLocation(minX, minY);
            
            Log.note("collision with corner at " + Box.toString(minID));
            
//            assert assertIsInScene(scene, inters, "before");
            
            getCorner(box, minID, circle, radius);
            circle.clipToOutside(inters);
            
            assert Tools.isReal(inters) : inters;
//            assert assertIsInScene(scene, inters, "after");
            assert !circle.contains(inters.x, inters.y);
            
            reflect(circle, inters, reflect);
            
            return true;
        }
        
        private boolean assertIsInScene(Scene scene, Point2D point, String label) {
            assert scene.getBox(this.sceneBox).contains(point.getX(), point.getY(), -getRadius())
                 : String.format("%s: sceneBox = %s, point = ( %f, %f )", label, this.sceneBox, point.getX(), point.getY());
            return true;
        }
        
        private void getCorner(Box box, int id, Circle circle, double radius) {
            var corner = box.getCorner(id, this.corner);
            circle.set(corner.getX(), corner.getY(), radius);
        }
        
        private void reflect(Circle      circle,
                             Point2D.Double col,
                             Point2D.Double vel) {
            var normal = this.normal;
            
            normal.x = col.x - circle.a;
            normal.y = col.y - circle.b;
            
            Tools.reflect(normal, vel);
        }
    }
    
    private final class PaddleCornerCollider extends BoxCornerCollider {
        @Override
        protected Box getBox(Scene scene, Box box) {
            return scene.getPaddleBox(box);
        }
    }
    
    private interface SingleBlockCollider extends Collider {
        void setBlock(BlockEntity block);
    }
    
    private final class BlockBoxCollider extends BoxCollider implements SingleBlockCollider {
        protected BlockEntity block;
        
        @Override
        public void setBlock(BlockEntity block) {
            this.block = block;
        }
        
        @Override
        protected boolean isInside() {
            return false;
        }
        @Override
        protected boolean includeSouthEdge() {
            return true;
        }
        @Override
        protected Box getBox(Scene scene, Box box) {
            assert block != null;
            return block.getBox(box);
        }
    }
    
    private final class BlockCornerCollider extends BoxCornerCollider implements SingleBlockCollider {
        protected BlockEntity block;
        
        @Override
        public void setBlock(BlockEntity block) {
            this.block = block;
        }
        
        @Override
        protected Box getBox(Scene scene, Box box) {
            assert block != null;
            return block.getBox(box);
        }
    }
    
    private final class BlockCollider implements Collider {
        private final BlockBoxCollider      boxCollider    = new BlockBoxCollider();
        private final BlockCornerCollider   cornerCollider = new BlockCornerCollider();
        private final SingleBlockCollider[] colliders      = {boxCollider, cornerCollider};
        
        private Scene       lastScene;
        private BlockEntity lastBlock;
        
        private static final double SPEED_MULTIPLIER = 1.0; // 1.0625; // didn't work so well
        
        @Override
        public boolean collide(Scene          scene,
                               Point2D.Double oldLoc,
                               Point2D.Double newLoc,
                               Point2D.Double inters,
                               Point2D.Double reflect) {
            var colliders = this.colliders;
            var intersX   = Double.MAX_VALUE;
            var intersY   = Double.MAX_VALUE;
            var minDistSq = Double.MAX_VALUE;
            var inVelX    = reflect.x;
            var inVelY    = reflect.y;
            var outVelX   = inVelX;
            var outVelY   = inVelY;
            
            for (var block : scene.getBlocks()) {
                var didCollide = false;
                
                for (var c : colliders) {
                    c.setBlock(block);
                    
                    reflect.x = inVelX;
                    reflect.y = inVelY;
                    
                    if (c.collide(scene, oldLoc, newLoc, inters, reflect)) {
                        var distSq = Tools.hypotSq(inters.x, inters.y);
                        
                        if (didCollide && minDistSq <= distSq)
                            continue;
                        
                        didCollide = true;
                        intersX    = inters.x;
                        intersY    = inters.y;
                        outVelX    = reflect.x;
                        outVelY    = reflect.y;
                        minDistSq  = distSq;
                    }
                }
                
                if (didCollide) {
                    this.lastScene = scene;
                    this.lastBlock = block;
                    inters.setLocation(intersX, intersY);
                    reflect.setLocation(outVelX * SPEED_MULTIPLIER,
                                        outVelY * SPEED_MULTIPLIER);
                    return true;
                }
            }
            
            return false;
        }
        
        @Override
        public void chosen() {
            lastScene.removeBlock(lastBlock);
        }
    }
    
    @Override
    protected void drawImpl(Game game, Graphics2D g) {
        var scene  = game.getScene();
        var bounds = this.bounds;
        var shape  = this.shape;
        
        var halfW = bounds.width / 2.0;
        var halfH = bounds.height / 2.0;
        
        shape.x = scene.xGuToPx(bounds.x - halfW);
        shape.y = scene.yGuToPx(bounds.y - halfH);
        
        shape.width = shape.height =
            (scene.xGuToPx(bounds.width) + scene.yGuToPx(bounds.height)) / 2.0;
        
        g.setColor(getColor());
        g.fill(shape);
        g.setColor(Color.BLACK);
        g.draw(shape);
        
        if (showVelocityVector) {
            var velocity = this.velocity;
            var vector   = this.debugVector;
            
            vector.x1 = scene.xGuToPx(bounds.x);
            vector.y1 = scene.yGuToPx(bounds.y);
            vector.x2 = scene.xGuToPx(bounds.x + velocity.x);
            vector.y2 = scene.yGuToPx(bounds.y + velocity.y);
            
            g.setColor(Colors.TRANS_LIGHT_GRAY);
            g.draw(vector);
        }
        
        if (showCollisions) {
            g.setColor(Colors.TRANS_RED);
            
            for (Point2D.Double p : collisions) {
                shape.x = scene.xGuToPx(p.x - halfW);
                shape.y = scene.yGuToPx(p.y - halfH);
                
                g.draw(shape);
            }
        }
    }
}