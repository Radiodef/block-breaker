package com.radiodef.blockbreaker.game;

import com.radiodef.blockbreaker.util.*;

import java.awt.*;
import java.awt.geom.*;

/**
 * @author David Staver
 */
public abstract class AbstractEntity<E extends AbstractEntity<E>> implements Entity {
    protected final Rectangle2D.Double bounds = new Rectangle2D.Double();
    
    protected final Point2D.Double velocity = new Point2D.Double();
    
    protected boolean isVisible = true;

    protected AbstractEntity() {
    }

    protected abstract E self();

    @Override
    public Point2D getLocation(Point2D result) {
        if (result == null)
            result = new Point2D.Double();
        Rectangle2D.Double bounds = this.bounds;
        result.setLocation(bounds.x, bounds.y);
        return result;
    }

    @Override
    public Rectangle2D getBounds(Rectangle2D result) {
        if (result == null)
            result = new Rectangle2D.Double();
        result.setRect(bounds);
        return result;
    }
    
    @Override
    public Box getBox(Box box) {
        return ((box == null) ? new Box() : box).set(bounds);
    }
    
    @Override
    public double getX() {
        return bounds.x;
    }
    
    @Override
    public double getY() {
        return bounds.y;
    }
    
    @Override
    public double getWidth() {
        return bounds.width;
    }
    
    @Override
    public double getHeight() {
        return bounds.height;
    }

    @Override
    public E setLocation(double x, double y) {
        Rectangle2D.Double bounds = this.bounds;
        bounds.x = x;
        bounds.y = y;
        return self();
    }
    
    @Override
    public Point2D getVelocity(Point2D result) {
        if (result == null)
            result = new Point2D.Double();
        result.setLocation(velocity);
        return result;
    }
    
    @Override
    public double getSpeed() {
        var velocity = this.velocity;
        return Math.hypot(velocity.y, velocity.x);
    }
    
    @Override
    public E setVelocity(double x, double y) {
        velocity.setLocation(x, y);
        return self();
    }
    
    @Override
    public boolean isVisible() {
        return isVisible;
    }
    
    @Override
    public E setVisible(boolean isVisible) {
        this.isVisible = isVisible;
        return self();
    }
    
    @Override
    public E toggleVisibility() {
        Entity.super.toggleVisibility();
        return self();
    }
    
    @Override
    public void draw(Game game, Graphics2D g) {
        if (isVisible()) {
            drawImpl(game, g);
        }
    }
    
    protected abstract void drawImpl(Game game, Graphics2D g);
}
