package com.radiodef.blockbreaker.game;

import com.radiodef.blockbreaker.util.*;

import java.awt.geom.*;

/**
 * @author David Staver
 */
public interface Entity extends GameObject, Visual {
    default Point2D getLocation() {
        return getLocation(null);
    }

    Point2D getLocation(Point2D locResult);

    default Rectangle2D getBounds() {
        return getBounds(null);
    }

    Rectangle2D getBounds(Rectangle2D boundsResult);
    
    default Box getBox(Box box) {
        if (box == null)
            box = new Box();
        return box.set(getBounds());
    }
    
    default double getX() {
        return getBounds().getX();
    }
    
    default double getY() {
        return getBounds().getY();
    }
    
    default double getWidth() {
        return getBounds().getWidth();
    }
    
    default double getHeight() {
        return getBounds().getHeight();
    }

    Entity setLocation(double x, double y);
    
    default Point2D getVelocity() {
        return getVelocity(null);
    }
    
    Point2D getVelocity(Point2D velResult);
    
    default double getSpeed() {
        var vel = getVelocity();
        return Math.hypot(vel.getX(), vel.getY());
    }
    
    Entity setVelocity(double x, double y);
    
    boolean isVisible();
    Entity setVisible(boolean isVisible);
    
    default Entity toggleVisibility() {
        return setVisible(!isVisible());
    }
}
