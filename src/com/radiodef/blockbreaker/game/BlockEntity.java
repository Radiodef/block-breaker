package com.radiodef.blockbreaker.game;

import com.radiodef.blockbreaker.util.*;

import java.awt.*;
import java.awt.geom.*;
import java.util.*;

/**
 * @author David Staver
 */
public class BlockEntity extends AbstractEntity<BlockEntity> implements ColoredVisual<BlockEntity> {
    public static final double WIDTH  = 3;
    public static final double HEIGHT = 1;
    
    private static final Color[] COLORS = {
        Color.RED,
        Color.BLUE,
        Color.YELLOW,
        Color.GREEN,
        Color.MAGENTA,
        Color.PINK,
        Color.CYAN,
        Color.ORANGE,
        Colors.PURPLE,
    };
    
    public static Color getRandomColor(Random rand) {
        return COLORS[rand.nextInt(COLORS.length)];
    }
    
    private final DebugHitBoxVisual hitBox = new DebugHitBoxVisual(this);
    
    private final Rectangle2D.Double temp = new Rectangle2D.Double();
    
    private Color color;
    
    BlockEntity() {
        this((Color) null);
    }
    
    BlockEntity(Color color) {
        setColor(color);
        
        bounds.width  = WIDTH;
        bounds.height = HEIGHT;
    }
    
    @Override
    protected BlockEntity self() {
        return this;
    }
    
    @Override
    public Color getColor() {
        return color;
    }
    
    @Override
    public BlockEntity setColor(Color color) {
        if (color == null) {
            color = Color.WHITE;
        }
        this.color = color;
        return this;
    }

    public BlockEntity setColor(Random rand) {
        return setColor(getRandomColor(rand));
    }
    
    @Override
    public BlockEntity setLocation(double x, double y) {
        super.setLocation(x, y);
        return this;
    }
    
    @Override
    public void update(Game game, double t) {
    }
    
    @Override
    protected void drawImpl(Game game, Graphics2D g) {
        var scene  = game.getScene();
        var bounds = this.bounds;
        var temp   = this.temp;
        
        temp.x      = scene.xGuToPx(bounds.x);
        temp.y      = scene.yGuToPx(bounds.y);
        temp.width  = scene.xGuToPx(bounds.width);
        temp.height = scene.yGuToPx(bounds.height);
        
        g.setColor(getColor());
        g.fill(temp);
        g.setColor(Color.BLACK);
        g.draw(temp);
        
        hitBox.draw(game, g);
    }
}
