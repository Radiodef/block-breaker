package com.radiodef.blockbreaker.game;

import java.awt.*;

/**
 * @author David Staver
 */
public interface Visual {
    void draw(Game game, Graphics2D g);
}
