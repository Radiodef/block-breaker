package com.radiodef.blockbreaker.game;

import com.radiodef.blockbreaker.util.*;

import java.awt.*;

/**
 * @author David Staver
 */
public class ScoreEntity extends TextEntity {
    private final Dimension sceneSize = new Dimension();
    
    ScoreEntity() {
        setToOverlay();
        setAnchor(Anchor.SW);
    }
    
    @Override
    public void update(Game game, double t) {
        var scene = game.getScene();
        
        if (game.getState() == State.MENU) {
            setText("-");
        } else {
            setText(scene.getScore().toString());
        }
        
        var sceneSize = scene.getSizeInGameUnits(this.sceneSize);
        setLocation(1, sceneSize.height - 1);
    }
}
