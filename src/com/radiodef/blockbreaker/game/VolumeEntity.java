package com.radiodef.blockbreaker.game;

import com.radiodef.blockbreaker.util.*;

import java.awt.*;
import javax.swing.*;

/**
 * @author David Staver
 */
public class VolumeEntity extends TextEntity {
    private static final int TIMEOUT = 3_000;
    
    private final Dimension sceneSize = new Dimension();
    
    private int lastVolume = -1;
    
    private final Timer timer;
    
    VolumeEntity() {
        setToOverlay();
        setAnchor(Anchor.SW);
        
        timer = new Timer(TIMEOUT, e -> {});
        timer.setRepeats(false);
        timer.setCoalesce(false);
    }
    
    @Override
    public void update(Game game, double t) {
        var volume = game.getAudioPlayer().getVolume();
        
        if (volume != lastVolume) {
            if (volume == 0) {
                setText("Volume: 0%");
            } else {
                setText("Volume: " + volume + "0%");
            }
            
            lastVolume = volume;
            timer.stop();
            timer.start();
        }
        
        var scene = game.getScene();
        var size  = scene.getSizeInGameUnits(sceneSize);
        setLocation(1, size.height - 3);
        
        boolean visible = timer.isRunning()
                       || game.getState() != State.PLAY
                       || scene.isInLaunchMode();
        setVisible(visible);
    }
}