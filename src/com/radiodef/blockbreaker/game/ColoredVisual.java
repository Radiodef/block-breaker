package com.radiodef.blockbreaker.game;

import java.awt.*;

/**
 * @author David Staver
 */
public interface ColoredVisual<V extends ColoredVisual<V>> extends Visual {
    Color getColor();
    V setColor(Color color);
}
