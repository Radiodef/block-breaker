package com.radiodef.blockbreaker.game;

import com.radiodef.blockbreaker.util.*;

import java.awt.*;
import java.awt.geom.*;
import java.util.*;

/**
 * @author David Staver
 */
public class DebugHitBoxVisual implements Visual {
    private final Box              box   = new Box();
    private final Line2D.Double    line  = new Line2D.Double();
    private final Point2D.Double   point = new Point2D.Double();
    private final Arc2D.Double     arc   = new Arc2D.Double(Arc2D.OPEN);
    
    private final Entity e;
    
    DebugHitBoxVisual(Entity e) {
        this.e = Objects.requireNonNull(e, "e");
    }
    
    @Override
    public void draw(Game game, Graphics2D g) {
        var scene = game.getScene();
        if (!scene.showHitBoxes())
            return;
        
        var stroke = g.getStroke();
        try {
            g.setStroke(Tools.DASHED_STROKE);
            g.setColor(Color.BLACK);
            
            var box      = e.getBox(this.box);
            var line     = this.line;
            var point    = this.point;
            var arc      = this.arc;
            var radius   = scene.getBallRadius();
            var diameter = 2 * radius;
            
            for (var i = Box.MIN_ID; i <= Box.MAX_ID; ++i) {
                box.getLine(i, line, radius);
                scene.lineGuToPx(line);
                
                g.draw(line);
            }
            
            arc.width  = scene.xGuToPx(diameter);
            arc.height = scene.yGuToPx(diameter);
            arc.extent = 90;
            
            for (var i = Box.MIN_CORNER_ID; i <= Box.MAX_CORNER_ID; ++i) {
                box.getCorner(i, point);
                
                arc.x = scene.xGuToPx(point.x - radius);
                arc.y = scene.yGuToPx(point.y - radius);
                
                switch (i) {
                    case Box.NE:
                        arc.start = 0;
                        break;
                    case Box.NW:
                        arc.start = 90;
                        break;
                    case Box.SW:
                        arc.start = 180;
                        break;
                    case Box.SE:
                        arc.start = 270;
                        break;
                }
                
                g.draw(arc);
            }
        } finally {
            g.setStroke(stroke);
        }
    }
}
