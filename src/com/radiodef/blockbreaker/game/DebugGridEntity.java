package com.radiodef.blockbreaker.game;

import com.radiodef.blockbreaker.main.*;
import com.radiodef.blockbreaker.util.*;

import java.awt.*;
import java.awt.geom.*;

/**
 * @author David Staver
 */
public class DebugGridEntity extends AbstractEntity<DebugGridEntity> {
    private final Line2D.Double      line        = new Line2D.Double();
    private final Dimension          sceneSize   = new Dimension();
    private final Rectangle2D.Double sceneBounds = new Rectangle2D.Double();
    
    DebugGridEntity() {
        setVisible(BlockBreaker.isDebug());
    }
    
    @Override
    protected DebugGridEntity self() {
        return this;
    }
    
    @Override
    public void update(Game game, double t) {
    }
    
    @Override
    protected void drawImpl(Game game, Graphics2D g) {
        var scene       = game.getScene();
        var line        = this.line;
        var sceneSize   = this.sceneSize;
        var sceneBounds = this.sceneBounds;
        
        scene.getSizeInGameUnits(sceneSize);
        scene.getLastBoundsOnPanel(sceneBounds);
        
        g.setColor(Colors.VERY_TRANS_LIGHT_GRAY);
        
        for (int x = 1; x < sceneSize.width; ++x) {
            line.x1 = line.x2 = scene.xGuToPx(x);
            line.y1 = 0;
            line.y2 = sceneBounds.height;
            g.draw(line);
        }
        
        for (int y = 1; y < sceneSize.height; ++y) {
            line.y1 = line.y2 = scene.yGuToPx(y);
            line.x1 = 0;
            line.x2 = sceneBounds.width;
            g.draw(line);
        }
    }
}