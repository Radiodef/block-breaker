package com.radiodef.blockbreaker.game;

import com.radiodef.blockbreaker.main.*;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import javax.swing.*;
import java.util.*;

/**
 * @author David Staver
 */
// NOTE:
//  If the scene/game instance ever changes
//  (because e.g. we had one Scene instance
//  and set the game.scene to another Scene
//  instance, which we don't do now), we need
//  to remove this from the panel's listeners.
final class DebugBallThrower extends MouseAdapter {
    private final Scene scene;
    
    private final Rectangle2D.Double sceneBounds = new Rectangle2D.Double();
    
    private Point press;
    private Point current;
    
    private boolean isAiming;
    private boolean wasStationary;
    
    private boolean isEnabled = BlockBreaker.isDebug();
    
    DebugBallThrower(Scene scene) {
        this.scene = Objects.requireNonNull(scene, "scene");
    }
    
    void toggleEnabled() {
        isEnabled = !isEnabled;
        
        if (!isEnabled) {
            releaseIfAiming();
        }
    }
    
    @Override
    public void mousePressed(MouseEvent e) {
        if (isEnabled && SwingUtilities.isLeftMouseButton(e)) {
            isAiming = true;
            press    = e.getPoint();
            current  = press;
            
            wasStationary = scene.isBallStationary();
            scene.setBallStationary(true);
            
            scene.getLastBoundsOnPanel(sceneBounds);
            var x = scene.xPxToGu(press.x - sceneBounds.x);
            var y = scene.yPxToGu(press.y - sceneBounds.y);
            
            scene.setBallLocation(x, y);
            scene.setBallVelocity(0, 0);
        }
    }
    
    @Override
    public void mouseDragged(MouseEvent e) {
        stopIfBallNotStationary();
        
        if (isAiming) {
            current = e.getPoint();
            setBallVelocity();
        }
    }
    
    @Override
    public void mouseReleased(MouseEvent e) {
        stopIfBallNotStationary();
        
        if (isAiming && SwingUtilities.isLeftMouseButton(e)) {
            current = e.getPoint();
            mouseReleased();
        }
    }
    
    // In the case of the following sequence:
    //  1. game is in launch mode
    //  2. mouse pressed to start aiming a vector
    //  3. ball is launched
    //  4. mouse is released
    // We don't want to set the ball to be stationary
    // again. We just want to cancel the aiming.
    private void stopIfBallNotStationary() {
        if (wasStationary && !scene.isBallStationary()) {
            isAiming = false;
        }
    }
    
    private void releaseIfAiming() {
        if (isAiming) {
            mouseReleased();
        }
    }
    
    private void mouseReleased() {
        isAiming = false;
        setBallVelocity();
        scene.setBallStationary(wasStationary);
    }
    
    private void setBallVelocity() {
        double x = scene.xPxToGu(current.x - press.x);
        double y = scene.yPxToGu(current.y - press.y);
        scene.setBallVelocity(x, y);
    }
}