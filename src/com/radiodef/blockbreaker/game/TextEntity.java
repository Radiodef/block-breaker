package com.radiodef.blockbreaker.game;

import com.radiodef.blockbreaker.util.*;

import java.awt.*;
import java.awt.geom.*;
import java.awt.font.*;

/**
 * @author David Staver
 */
public class TextEntity
extends    AbstractEntity<TextEntity>
implements ColoredVisual<TextEntity> {
    public enum Anchor {
        C, SE, SW, NW, NE;
    }
    
    private String text;
    private Color  color;
    private String family;
    private double size;
    private Anchor anchor;
    
    private TextShapeStore store;
    
    TextEntity() {
        setText(null).setColor(null).setFamily(null).setSize(1).setAnchor(null);
    }
    
    protected void setToOverlay() {
        setSize(1.5);
        setColor(Colors.TRANS_BLACK);
    }
    
    public static TextEntity newSimpleInfoText() {
        return new TextEntity().setColor(Color.BLACK)
                               .setSize(1.0)
                               .setAnchor(Anchor.C);
    }
    
    public static TextEntity newSimpleMenuText() {
        return newSimpleInfoText().setText("Press 'N' to start a new game!");
    }
    
    public static TextEntity newSimplePauseText() {
        return newSimpleInfoText().setText("Paused. Press 'Escape' to unpause!");
    }
    
    public static TextEntity newSimpleLaunchText() {
        final String lines = "Controls:" + "\n"
                           + " • 'Space' to launch the ball." + "\n"
                           + " • 'A' or left arrow to move the paddle left." + "\n"
                           + " • 'D' or right arrow to move the paddle right." + "\n"
                           + " • 'Escape' to pause and unpause." + "\n"
                           + " • Numbers 0-9 to set the sound volume.";
        return newSimpleInfoText().setText(lines).setSize(0.75).setAnchor(Anchor.C);
    }
    
    @Override
    protected TextEntity self() {
        return this;
    }
    
    @Override
    public Color getColor() {
        return color;
    }
    
    @Override
    public TextEntity setColor(Color color) {
        if (color == null) {
            color = Color.BLACK;
        }
        this.color = color;
        return this;
    }
    
    private void clearStore() {
        store = null;
    }
    
    public String getText() {
        return text;
    }
    
    public TextEntity setText(String text) {
        this.text = text;
        clearStore();
        return this;
    }
    
    public double getSize() {
        return size;
    }
    
    public TextEntity setSize(double size) {
        if (size <= 0) {
            throw new IllegalArgumentException("size = " + size);
        }
        this.size = size;
        clearStore();
        return this;
    }
    
    public String getFamily() {
        return family;
    }
    
    public TextEntity setFamily(String family) {
        if (family == null) {
            family = Font.MONOSPACED;
        }
        this.family = family;
        clearStore();
        return this;
    }
    
    public Anchor getAnchor() {
        return anchor;
    }
    
    public TextEntity setAnchor(Anchor anchor) {
        if (anchor == null) {
            anchor = Anchor.C;
        }
        this.anchor = anchor;
        return this;
    }
    
    public TextEntity centerLocationIn(Dimension2D size) {
        return setLocation(size.getWidth() / 2.0, size.getHeight() / 2.0);
    }
    
    @Override
    public void update(Game game, double t) {
    }
    
    @Override
    protected void drawImpl(Game game, Graphics2D g) {
        var text = this.text;
        if (text != null && !text.isEmpty()) {
            try (GraphicsRef ref = new GraphicsRef(g)) {
                actuallyDraw(game, ref.get());
            }
        }
    }
    
    private void actuallyDraw(Game game, Graphics2D g) {
        var scene = game.getScene();
        
        var store = this.store;
        if (store == null || scene.sizeChanged()) {
            this.store = store = new TextShapeStore(this, game, g);
        }
        
        var count = store.count;
        if (count == 0) {
            return;
        }
        
        g.setFont(store.font);
        g.setColor(getColor());
        
        var bounds = this.bounds;
        var x = scene.xGuToPx(bounds.x);
        var y = scene.yGuToPx(bounds.y);
        
        var scale  = store.scale;
        var height = store.height;
        var width  = store.width;
        
        switch (getAnchor()) {
            case C:
                x -= width * scale / 2.0;
                y -= (height * count * scale / 2.0) - (height * scale);
                break;
            case SE:
                x -= width * scale;
                y -= height * (count - 1) * scale;
                break;
            case SW:
                y -= height * (count - 1) * scale;
                break;
            case NW:
                y += height * scale;
                break;
            case NE:
                x -= width * scale;
                y += height * scale;
                break;
            default:
                assert false : this;
                break;
        }
        
        g.translate(x, y);
        g.scale(scale, scale);
        
        for (var outline : store.outlines) {
            g.fill(outline);
        }
    }
    
    private static final class TextShapeStore {
        static final int BASE_SIZE = 64;
        
        final Font          font;
        final GlyphVector[] vectors;
        final Rectangle2D[] bounds;
        final Shape[]       outlines;
        final int           count;
        final double        scale;
        final double        width;
        final double        height;
        
        TextShapeStore(TextEntity e, Game game, Graphics2D g) {
            this(e, e.getText(), game, g);
        }
        
        TextShapeStore(TextEntity e, String text, Game game, Graphics2D g) {
            var scene   = game.getScene();
            var sizeGu  = e.getSize();
            var sizePx  = scene.yGuToPx(sizeGu);
            var font    = new Font(e.getFamily(), Font.PLAIN, BASE_SIZE);
            var metrics = g.getFontMetrics(font);
            var scale   = sizePx / metrics.getAscent();
            var height  = metrics.getHeight();
            
            this.font   = font;
            this.scale  = scale;
            this.height = height;
            
            var lines = text.split("\\n");
            var count = lines.length;
            
            this.count = count;
            
            var vectors  = new GlyphVector[count];
            var bounds   = new Rectangle2D[count];
            var outlines = new Shape[count];
            var width    = 0.0;
            
            for (var i = 0; i < count; ++i) {
                var vector  = font.createGlyphVector(g.getFontRenderContext(), lines[i]);
                vectors[i]  = vector;
                bounds[i]   = vector.getLogicalBounds();
                width       = Math.max(width, bounds[i].getWidth());
                var outline = vector.getOutline();
                if (i == 0) {
                    outlines[i] = outline;
                } else {
                    outlines[i] = AffineTransform.getTranslateInstance(0, height * i).createTransformedShape(outline);
                }
            }
            
            this.vectors  = vectors;
            this.bounds   = bounds;
            this.outlines = outlines;
            this.width    = width;
        }
    }
    
    static {
        initClasses();
    }
    
    private static void initClasses() {
        // these classes seemed to take awhile to load,
        // so we don't want to do it lazily
        Font f = new Font(Font.SERIF, Font.PLAIN, 12);
        FontRenderContext frc = new FontRenderContext(new AffineTransform(), true, true);
        GlyphVector v = f.createGlyphVector(frc, "AaBbCc");
    }
    
    @Override
    public String toString() {
        return "TextEntity{text = \"" + getText() + "\", anchor = " + getAnchor() + "}";
    }
}
