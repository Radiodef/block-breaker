package com.radiodef.blockbreaker.game;

import com.radiodef.blockbreaker.util.*;
import com.radiodef.blockbreaker.app.*;

import java.awt.*;
import java.awt.event.*;

import java.util.*;
import java.util.List;
import java.util.function.*;

/**
 * @author David Staver
 */
public final class Controls implements AutoCloseable {
    private final Game game;
    
    private final KeyboardFocusManager manager;
    
    private final Dispatcher dispatcher;
    
    private final Set<Integer> keysDown;
    
    private Set<Integer> keysDownLastFrame;
    private Set<Integer> keysDownThisFrame;
    
    private final Map<Integer, Binding> bindings;
    
    Controls(Game game) {
        Tools.requireEDT();
        this.game = Objects.requireNonNull(game, "game");
        
        manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        
        for (int id : List.of(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS,
                              KeyboardFocusManager.BACKWARD_TRAVERSAL_KEYS,
                              KeyboardFocusManager.UP_CYCLE_TRAVERSAL_KEYS,
                              KeyboardFocusManager.DOWN_CYCLE_TRAVERSAL_KEYS)) {
            manager.setDefaultFocusTraversalKeys(id, Collections.emptySet());
        }
        
        dispatcher = new Dispatcher();
        keysDown   = new LinkedHashSet<>();
        
        keysDownLastFrame = new LinkedHashSet<>();
        keysDownThisFrame = new LinkedHashSet<>();
        
        bindings = makeBindings();
        
        manager.addKeyEventDispatcher(dispatcher);
    }
    
    @Override
    public void close() {
        manager.removeKeyEventDispatcher(dispatcher);
    }
    
    private final class Dispatcher implements KeyEventDispatcher {
        @Override
        public boolean dispatchKeyEvent(KeyEvent e) {
            int id = e.getID();
            if (id != KeyEvent.KEY_TYPED) {
                Integer keyCode = e.getKeyCode();
                switch (id) {
                    case KeyEvent.KEY_PRESSED: {
                        if (keysDown.add(keyCode)) {
                            Log.note("pressed " + Tools.getKeyCodeName(keyCode));
                            Binding binding = bindings.get(keyCode);
                            if (binding != null) {
                                if (!binding.isDebugBinding() || isDebugModifierDown()) {
                                    binding.accept(game);
                                }
                            }
                        }
                        break;
                    }
                    case KeyEvent.KEY_RELEASED: {
                        if (keysDown.remove(keyCode)) {
                            Log.note("released " + Tools.getKeyCodeName(keyCode));
                        }
                        break;
                    }
                }
            }
            return false;
        }
    }
    
    public boolean isDebugModifierDown() {
        return isDown(DEBUG_MODIFIER_KEY);
    }
    
    public boolean isDown(Integer keyCode) {
        Tools.requireEDT();
        return keysDownThisFrame.contains(keyCode);
    }
    
    public boolean anyDown(List<Integer> keyCodes) {
        Tools.requireEDT();
        for (int i = 0, size = keyCodes.size(); i < size; ++i)
            if (isDown(keyCodes.get(i)))
                return true;
        return false;
    }
    
    // unused now; use bindings in a map instead
    /*
    public boolean wasPressed(Integer keyCode) {
        Tools.requireEDT();
        return !keysDownLastFrame.contains(keyCode) && keysDownThisFrame.contains(keyCode);
    }
    
    public boolean anyPressed(List<Integer> keyCodes) {
        Tools.requireEDT();
        for (int i = 0, size = keyCodes.size(); i < size; ++i)
            if (wasPressed(keyCodes.get(i)))
                return true;
        return false;
    }
    */
    
    void markFrame() {
        Tools.requireEDT();
        
        Set<Integer> temp = keysDownThisFrame;
        keysDownThisFrame = keysDownLastFrame;
        keysDownLastFrame = temp;
        
        keysDownThisFrame.clear();
        keysDownThisFrame.addAll(keysDown);
    }
    
    public static final Integer NEW_GAME_KEY = KeyEvent.VK_N;
    public static final Integer PAUSE_KEY    = KeyEvent.VK_ESCAPE;
    public static final Integer LAUNCH_KEY   = KeyEvent.VK_SPACE;
    
    public static final List<Integer> LEFT_KEYS  = List.of(KeyEvent.VK_LEFT,  KeyEvent.VK_A);
    public static final List<Integer> RIGHT_KEYS = List.of(KeyEvent.VK_RIGHT, KeyEvent.VK_D);
    
    public static final Integer DEBUG_MODIFIER_KEY        = KeyEvent.VK_CONTROL;
    public static final Integer DEBUG_CROSS_KEY           = KeyEvent.VK_X;
    public static final Integer DEBUG_GRID_KEY            = KeyEvent.VK_G;
    public static final Integer DEBUG_FRAME_COUNT_KEY     = KeyEvent.VK_F;
    public static final Integer DEBUG_VELOCITY_VECTOR_KEY = KeyEvent.VK_V;
    public static final Integer DEBUG_MOUSE_THROWER_KEY   = KeyEvent.VK_T;
    public static final Integer DEBUG_SOUTH_WALL_KEY      = KeyEvent.VK_W;
    public static final Integer DEBUG_COLLISIONS_KEY      = KeyEvent.VK_C;
    public static final Integer DEBUG_HIT_BOXES_KEY       = KeyEvent.VK_H;
    
    public static final List<Integer> VOLUME_KEYS = List.of(KeyEvent.VK_0,
                                                            KeyEvent.VK_1,
                                                            KeyEvent.VK_2,
                                                            KeyEvent.VK_3,
                                                            KeyEvent.VK_4,
                                                            KeyEvent.VK_5,
                                                            KeyEvent.VK_6,
                                                            KeyEvent.VK_7,
                                                            KeyEvent.VK_8,
                                                            KeyEvent.VK_9);
    
    private static Map<Integer, Binding> makeBindings() {
        Map<Integer, Binding> m = new LinkedHashMap<>();
        
        // Note:
        //  The paddle controls are processed in com.radiodef.blockbreaker.game.Loop,
        //  because they use polling instead of this command pattern.
        //  Polling (that is, processing the control state every frame) is more stable
        //  for things like unpausing while holding down a movement key. The down-side
        //  to polling is that you will miss quick key presses if the game's frame rate
        //  drops too low.
        
        m.put(NEW_GAME_KEY, Game::startNewGame);
        
        m.put(PAUSE_KEY, game -> {
            switch (game.getState()) {
                case MENU:
                    // do nothing
                    break;
                case PLAY:
                    game.setState(State.PAUSE);
                    break;
                case PAUSE:
                    game.setState(State.PLAY);
                    break;
            }
        });
        
        m.put(LAUNCH_KEY, game -> {
            if (game.getState() == State.PLAY) {
                game.getScene().launchIfInLaunchMode();
            }
        });
        
        m.put(DEBUG_FRAME_COUNT_KEY,     debugToggle(Scene::toggleDebugFrameCountVisibility));
        m.put(DEBUG_CROSS_KEY,           debugToggle(Scene::toggleDebugCrossVisibility));
        m.put(DEBUG_GRID_KEY,            debugToggle(Scene::toggleDebugGridVisibility));
        m.put(DEBUG_VELOCITY_VECTOR_KEY, debugToggle(Scene::toggleDebugVelocityVectorVisibility));
        m.put(DEBUG_MOUSE_THROWER_KEY,   debugToggle(Scene::toggleDebugBallThrowerEnabled));
        m.put(DEBUG_SOUTH_WALL_KEY,      debugToggle(Scene::toggleDebugSouthWallEnabled));
        m.put(DEBUG_COLLISIONS_KEY,      debugToggle(Scene::toggleDebugCollisionsVisibility));
        m.put(DEBUG_HIT_BOXES_KEY,       debugToggle(Scene::toggleDebugHitBoxesVisibility));
        
        for (Integer key : VOLUME_KEYS) {
            int volume = toInt(key);
            m.put(key, g -> g.getAudioPlayer().setVolume(volume));
        }
        
        return m;
    }
    
    private static DebugBinding debugToggle(Consumer<Scene> action) {
        return game -> action.accept(game.getScene());
    }
    
    @FunctionalInterface
    private interface Binding extends Consumer<Game> {
        default boolean isDebugBinding() {
            return false;
        }
    }
    @FunctionalInterface
    private interface DebugBinding extends Binding {
        @Override
        default boolean isDebugBinding() {
            return true;
        }
    }
    
    public static int toInt(int keyCode) {
        switch (keyCode) {
            case KeyEvent.VK_0:
                return 0;
            case KeyEvent.VK_1:
                return 1;
            case KeyEvent.VK_2:
                return 2;
            case KeyEvent.VK_3:
                return 3;
            case KeyEvent.VK_4:
                return 4;
            case KeyEvent.VK_5:
                return 5;
            case KeyEvent.VK_6:
                return 6;
            case KeyEvent.VK_7:
                return 7;
            case KeyEvent.VK_8:
                return 8;
            case KeyEvent.VK_9:
                return 9;
            default:
                throw new IllegalArgumentException(Tools.getKeyCodeName(keyCode));
        }
    }
}