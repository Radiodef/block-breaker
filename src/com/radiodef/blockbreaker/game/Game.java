package com.radiodef.blockbreaker.game;

import com.radiodef.blockbreaker.app.*;
import com.radiodef.blockbreaker.audio.*;

import java.util.*;

/**
 * @author David Staver
 */
public final class Game implements AutoCloseable {
    private final GameWindow window;
    
    private final Controls controls;
    
    private final Loop loop;
    
    private final AudioPlayer player;
    
    private final Random random;
    
    private final Scene scene;
    
    private State state;
    
    public Game() {
        this(new GameWindow());
    }
    
    private Game(GameWindow window) {
        Log.enter();
        
        this.window = Objects.requireNonNull(window, "window");
        window.setGame(this);
        
        random   = new Random();
        controls = new Controls(this);
        state    = State.MENU;
        scene    = new Scene(this);
        loop     = new Loop(this);
        player   = new AudioPlayer().start();
        
        scene.resetScene(random);
    }
    
    @Override
    public void close() {
        Log.enter();
        controls.close();
        loop.stop();
        player.stop();
    }
    
    public Random getRandom() {
        return random;
    }
    
    public AudioPlayer getAudioPlayer() {
        return player;
    }
    
    public State getState() {
        return state;
    }
    
    public void setState(State state) {
        Objects.requireNonNull(state, "state");
        Log.note(state);
        
        if (this.state != state) {
            this.state = state;
            
            switch (state) {
                case MENU:
                    break;
                case PLAY:
                    break;
                case PAUSE:
                    break;
            }
        }
    }
    
    public void startNewGame() {
        Log.note("starting a new game");
        scene.startNewScene(this);
        setState(State.PLAY);
        loop.restart();
    }
    
    public void startLoop() {
        loop.start();
    }
    
    public Controls getControls() {
        return controls;
    }
    
    public GameWindow getWindow() {
        return window;
    }
    
    public Scene getScene() {
        return scene;
    }
    
    Loop getLoop() {
        return loop;
    }
}
