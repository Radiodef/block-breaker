package com.radiodef.blockbreaker.game;

import com.radiodef.blockbreaker.app.*;
import com.radiodef.blockbreaker.util.*;
import com.radiodef.blockbreaker.main.*;
import com.radiodef.blockbreaker.audio.*;

import java.awt.*;
import java.awt.geom.*;
import java.util.*;
import java.util.List;
import java.math.*;

/**
 * @author David Staver
 */
public class Scene implements Visual, GameObject {
    public static Dimension getDefaultSizeInGameUnits() {
        return new Dimension(32, 24);
    }
    
    public static Dimension getDefaultAspectRatio() {
        return Tools.toAspectRatio(getDefaultSizeInGameUnits());
    }
    
    private static final Sound BALL_LOST_SOUND =
        new Synth().setKind(Synth.Kind.SAW)
                   .setAmplitude(0.35)
                   .setNotes("D1")
                   .setFade(0.03125)
                   .getSound(0.25);
    private static final Sound BLOCKS_CLEARED_SOUND;
    static {
        Synth synth =
            new Synth().setKind(Synth.Kind.SIN)
                       .setAmplitude(0.25)
                       .setFade(1.0 - 0.0625);
        BLOCKS_CLEARED_SOUND =
            new Sequence().add(0.0000, synth.setNotes("C5").getSamples(0.5))
                          .add(0.0625, synth.setNotes("G5").getSamples(0.75))
                          .add(0.0625, synth.setNotes("C6").getSamples(1.0))
                          .add(0.0625, synth.setNotes("G6").getSamples(1.0))
                          .add(0.0625, synth.setNotes("C7").getSamples(1.0))
                          .getSound();
    }
    
    // geometry
    private final Dimension sizeInGameUnits = getDefaultSizeInGameUnits();
    private final Rectangle2D.Double boundsOnPanel = new Rectangle2D.Double();
    
    // used as local variables in draw(...)
    private final Line2D.Double wallLine = new Line2D.Double();
    private final Box sceneBox = new Box();
    
    // flags
    private boolean showHitBoxes = BlockBreaker.isDebug();

    private boolean sizeChanged = false;
    
    private boolean isInLaunchMode = true;
    
    // misc
    private final DebugBallThrower debugThrower;
    
    private BigInteger score = BigInteger.ZERO;
    private BigInteger wins  = BigInteger.ZERO;
    
    // entities
    private final TextEntity menuText;
    private final TextEntity pauseText;
    private final TextEntity launchText;
    private final TextEntity scoreText;
    private final TextEntity volumeText;
    
    private final List<BlockEntity> blocks;
    
    private final BallEntity ball;
    private final PaddleEntity paddle;
    
    private final DebugFrameCountEntity debugFrameCount;
    private final DebugCrossEntity debugCross;
    private final DebugGridEntity debugGrid;
    
    private final List<Entity> debugEntities;

    Scene(Game game) {
        Log.enter();
        Objects.requireNonNull(game, "game");
        
        menuText        = TextEntity.newSimpleMenuText().centerLocationIn(sizeInGameUnits);
        pauseText       = TextEntity.newSimplePauseText().centerLocationIn(sizeInGameUnits);
        launchText      = TextEntity.newSimpleLaunchText().centerLocationIn(sizeInGameUnits);
        scoreText       = new ScoreEntity();
        volumeText      = new VolumeEntity();
        blocks          = new ArrayList<>();
        ball            = new BallEntity();
        paddle          = new PaddleEntity();
        
        debugFrameCount = new DebugFrameCountEntity();
        debugCross      = new DebugCrossEntity();
        debugGrid       = new DebugGridEntity();
        debugEntities   = List.of(debugGrid, debugCross, debugFrameCount);
        
        debugThrower    = new DebugBallThrower(this);
        
        var panel = game.getWindow().getPanel();
        panel.addMouseListener(debugThrower);
        panel.addMouseMotionListener(debugThrower);
    }
    
    public Dimension getSizeInGameUnits(Dimension sizeResult) {
        if (sizeResult == null) {
            sizeResult = new Dimension();
        }
        
        sizeResult.setSize(sizeInGameUnits);
        return sizeResult;
    }
    
    public double xPxPerGu() {
        return boundsOnPanel.width / sizeInGameUnits.width;
    }
    
    public double yPxPerGu() {
        return boundsOnPanel.height / sizeInGameUnits.height;
    }
    
    public double xGuToPx(double xGu) {
        return xGu * xPxPerGu();
    }
    
    public double yGuToPx(double yGu) {
        return yGu * yPxPerGu();
    }
    
    public double xPxToGu(double xPx) {
        return xPx / xPxPerGu();
    }
    
    public double yPxToGu(double yPx) {
        return yPx / yPxPerGu();
    }
    
    public Line2D lineGuToPx(Line2D line) {
        line.setLine(xGuToPx(line.getX1()),
                     yGuToPx(line.getY1()),
                     xGuToPx(line.getX2()),
                     yGuToPx(line.getY2()));
        return line;
    }
    
    public Rectangle2D getBoundsOn(Dimension sizeInPixels, Rectangle2D bounds) {
        if (bounds == null) {
            bounds = new Rectangle2D.Double();
        }
        
        Dimension sizeInGameUnits = this.sizeInGameUnits;
        
        double guW = sizeInGameUnits.width;
        double guH = sizeInGameUnits.height;
        double pxW = sizeInPixels.width;
        double pxH = sizeInPixels.height;
        
        double x, y, w, h;
        
        double aspectRatio = guW / guH;
        
        if (pxW > (pxH * aspectRatio)) {
            double pxPerGu = pxH / guH;
            w = pxPerGu * guW;
            h = pxH;
            x = (pxW / 2) - (w / 2);
            y = 0;
        } else {
            double pxPerGu = pxW / guW;
            w = pxW;
            h = pxPerGu * guH;
            x = 0;
            y = (pxH / 2) - (h / 2);
        }
        
        bounds.setRect(x, y, w, h);
        return bounds;
    }
    
    public Rectangle2D getLastBoundsOnPanel(Rectangle2D boundsResult) {
        if (boundsResult == null) {
            boundsResult = new Rectangle2D.Double();
        }
        boundsResult.setRect(boundsOnPanel);
        return boundsResult;
    }
    
    public Box getBox(Box result) {
        if (result == null) {
            result = new Box();
        }
        result.set(sizeInGameUnits);
        return result;
    }
    
    public BigInteger getScore() {
        return score;
    }
    
    public boolean sizeChanged() {
        return sizeChanged;
    }
    
    public boolean showHitBoxes() {
        return showHitBoxes;
    }
    
    public void toggleDebugHitBoxesVisibility() {
        showHitBoxes = !showHitBoxes;
    }
    
    public void toggleDebugFrameCountVisibility() {
        debugFrameCount.toggleVisibility();
    }
    
    public void toggleDebugCrossVisibility() {
        debugCross.toggleVisibility();
    }
    
    public void toggleDebugGridVisibility() {
        debugGrid.toggleVisibility();
    }
    
    public void toggleDebugVelocityVectorVisibility() {
        ball.toggleVelocityVector();
    }
    
    public void toggleDebugCollisionsVisibility() {
        ball.toggleShowCollisions();
    }
    
    public void toggleDebugBallThrowerEnabled() {
        debugThrower.toggleEnabled();
    }
    
    public void toggleDebugSouthWallEnabled() {
        ball.toggleBounceOffSouthWall();
    }
    
    public void setPaddleVelocity(boolean left, boolean right) {
        paddle.setVelocity(left, right);
    }
    
    public boolean isBallStationary() {
        return ball.isStationary();
    }
    
    public void setBallStationary(boolean isStationary) {
        ball.setStationary(isStationary);
    }
    
    public void setBallVelocity(double x, double y) {
        ball.setVelocity(x, y);
    }
    
    public void setBallLocation(double x, double y) {
        ball.setLocation(x, y);
    }
    
    public double getBallRadius() {
        return ball.getRadius();
    }
    
    public boolean removeBlock(BlockEntity b) {
        if (blocks.remove(b)) {
            var asDouble = ball.getSpeed();
            if (Double.isInfinite(asDouble))
                asDouble = Double.MAX_VALUE;
            var asDecimal = BigDecimal.valueOf(asDouble);
            var asInteger = asDecimal.multiply(Tools.BIG_ONE_HUNDRED).toBigInteger();
            if (asInteger.compareTo(BigInteger.ONE) < 0)
                asInteger = BigInteger.ONE;
            score = score.add(asInteger);
            return true;
        }
        return false;
    }
    
    public Iterable<BlockEntity> getBlocks() {
        return blocks;
    }
    
    public void startNewScene(Game game) {
        BLOCKS_CLEARED_SOUND.play(game.getAudioPlayer());
        resetScene(game.getRandom());
    }
    
    public void resetScene(Random rand) {
        resetScore();
        generateBlocks(rand);
        enterLaunchMode();
    }
    
    private void resetScore() {
        score = BigInteger.ZERO;
        wins  = BigInteger.ZERO;
    }
    
    private void generateBlocks(Random rand) {
        blocks.clear();
        
        int sceneWidth = sizeInGameUnits.width;
        
        double margin  = 1.0;
        double space   = 0.5;
        double epsilon = 1.0 / 1024.0;
        int    xCount  = 1;
        int    yCount  = 4;
        
        for (int x = xCount;; ++x, ++xCount) {
            double xLoc = margin + x * BlockEntity.WIDTH;
            if (x > 0) xLoc += x * space;
            
            if ((xLoc + BlockEntity.WIDTH - epsilon) > (sceneWidth - margin)) {
                break;
            }
        }
        
        double extraSpace = sceneWidth
                          - (2 * margin)
                          - (xCount * BlockEntity.WIDTH);
        space = extraSpace / (xCount - 1);
        
        for (int x = 0; x < xCount; ++x) {
            for (int y = 0; y < yCount; ++y) {
                BlockEntity e = new BlockEntity();
                e.setColor(rand);
                
                double xLoc = margin + x * BlockEntity.WIDTH;
                double yLoc = margin + y * BlockEntity.HEIGHT;
                
                if (x > 0) xLoc += x * space;
                if (y > 0) yLoc += y * space;
                
                e.setLocation(xLoc, yLoc);
                blocks.add(e);
            }
        }
    }
    
    private void enterLaunchMode() {
        isInLaunchMode = true;
        
        paddle.reset(this);
        ball.reset(this);
    }
    
    public boolean isInLaunchMode() {
        return isInLaunchMode;
    }
    
    public void launchIfInLaunchMode() {
        if (isInLaunchMode) {
            Log.note("launching");
            isInLaunchMode = false;
            ball.launch();
        }
    }
    
    public Rectangle2D getPaddleBounds(Rectangle2D result) {
        return paddle.getBounds(result);
    }
    
    public Box getPaddleBox(Box result) {
        return paddle.getBox(result);
    }
    
    @Override
    public void update(Game game, double elapsed) {
        var state = game.getState();
        
        if (state == State.PLAY) {
            for (var e : blocks) {
                e.update(game, elapsed);
            }
            
            paddle.update(game, elapsed);
            ball.update(game, elapsed);
        }
        
        for (var e : debugEntities) {
            e.update(game, elapsed);
        }
        
        if ((ball.getY() - ball.getRadius()) > sizeInGameUnits.height) {
            Log.note("ball lost");
            BALL_LOST_SOUND.play(game.getAudioPlayer());
            
            // TODO: add lives?
            resetScene(game.getRandom());
        }
        
        if (blocks.isEmpty()) {
            Log.note("cleared");
            BLOCKS_CLEARED_SOUND.play(game.getAudioPlayer());
            
            wins  = wins.add(BigInteger.ONE);
            score = score.multiply(wins.add(BigInteger.ONE));
            
            generateBlocks(game.getRandom());
            enterLaunchMode();
        }
        
        scoreText.update(game, elapsed);
        volumeText.update(game, elapsed);
    }
    
    @Override
    public void draw(Game game, Graphics2D g) {
        var bounds   = this.boundsOnPanel;
        var widthB4  = bounds.width;
        var heightB4 = bounds.height;
        
        getBoundsOn(game.getWindow().getPanel().getSize(), bounds);
        sizeChanged = (widthB4 != bounds.width) || (heightB4 != bounds.height);
        
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                           RenderingHints.VALUE_ANTIALIAS_ON);
        g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
                           RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        
        g.setColor(Color.WHITE);
        g.fill(bounds.getBounds());
        
        g.translate(bounds.getX(), bounds.getY());
        
        for (var e : blocks) {
            e.draw(game, g);
        }
        
        paddle.draw(game, g);
        ball.draw(game, g);
        
        if (showHitBoxes()) {
            var stroke = g.getStroke();
            try {
                g.setStroke(Tools.DASHED_STROKE);
                g.setColor(Color.BLACK);
                
                var box   = getBox(this.sceneBox);
                var line  = this.wallLine;
                var inset = -getBallRadius();
                
                for (var i = Box.MIN_ID; i <= Box.MAX_ID; ++i) {
                    g.draw(lineGuToPx(box.getLine(i, line, inset)));
                }
            } finally {
                g.setStroke(stroke);
            }
        }
        
        var state = game.getState();
        
        if (state == State.MENU) {
            menuText.draw(game, g);
        }
        if (state == State.PAUSE) {
            pauseText.draw(game, g);
        }
        if (state == State.PLAY && isInLaunchMode) {
            launchText.draw(game, g);
        }
        
        scoreText.draw(game, g);
        volumeText.draw(game, g);
        
        for (var e : debugEntities) {
            e.draw(game, g);
        }
    }
}
