package com.radiodef.blockbreaker.game;

import com.radiodef.blockbreaker.main.*;

import java.awt.*;

/**
 * @author David Staver
 */
public class DebugFrameCountEntity extends TextEntity {
    private final Dimension sceneSize = new Dimension();
    
    DebugFrameCountEntity() {
        this.setAnchor(Anchor.SE)
            .setSize(1)
            .setColor(Color.LIGHT_GRAY)
            .setVisible(BlockBreaker.isDebug());
    }
    
    @Override
    public void update(Game game, double elapsed) {
        super.update(game, elapsed);
        
        if (isVisible()) {
            var count = game.getLoop().getFrameCount();
            setText(Integer.toString(count));
            
            var scene = game.getScene();
            var size  = scene.getSizeInGameUnits(sceneSize);
            setLocation(size.width - 1, size.height - 1);
        }
    }
}
