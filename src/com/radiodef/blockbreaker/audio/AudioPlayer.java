package com.radiodef.blockbreaker.audio;

import com.radiodef.blockbreaker.app.*;

import javax.sound.sampled.*;
import java.util.*;

/**
 * @author David Staver
 */
public final class AudioPlayer {
    // static constants
    private static final double CIRCLE_BUFFER_SECONDS = 10.0;
    
    private static final int WRITE_BUFFER_FRAMES = 1024;
    
    public static final int SAMPLE_RATE = deconstant(44100);
    
    public static final int BIT_DEPTH = deconstant(16);
    
    private static int deconstant(int n) { return n; }
    
    // instance
    private final PlaybackLoop loop;
    private final Thread thread;
    
    private final Object monitor;
    
    private final AudioFormat fmt;
    
    private final byte[] circleBuf;
    private final byte[] writeBuf;
    
    private volatile int insert;
    private volatile int volume;
    private volatile boolean isRunning;
    
    private final SourceDataLine line;
    
    public AudioPlayer() {
        Log.enter();
        
        loop      = new PlaybackLoop();
        thread    = new Thread(loop);
        thread.setDaemon(false);
        monitor   = new Object();
        fmt       = new AudioFormat((float) SAMPLE_RATE, BIT_DEPTH, 1, true, true);
        circleBuf = new byte[getByteCount(fmt, CIRCLE_BUFFER_SECONDS)];
        writeBuf  = new byte[WRITE_BUFFER_FRAMES * fmt.getFrameSize()];
        insert    = 0;
        isRunning = true;
        volume    = 2;
        
        SourceDataLine line;
        try {
            line = AudioSystem.getSourceDataLine(fmt);
            line.open(fmt, writeBuf.length);
        } catch (LineUnavailableException x) {
            Log.caught(x);
            line = null;
        }
        
        this.line = line;
        
        // Note: We use a shutdown hook just for simplicity.
        //       This also responds to e.g. the Mac OS ⌘Q
        //       without too much hassle.
        Runtime.getRuntime().addShutdownHook(new Thread(this::stop));
    }
    
    public AudioPlayer start() {
        Log.enter();
        thread.start();
        return this;
    }
    
    public AudioPlayer stop() {
        Log.enter();
        isRunning = false;
        try {
            thread.join();
        } catch (InterruptedException x) {
            Log.caught(x);
        }
        return this;
    }
    
    public int getVolume() {
        return volume;
    }
    
    public AudioPlayer setVolume(int volume) {
        if (volume < 0 || 10 < volume) {
            throw new IllegalArgumentException("volume = " + volume);
        }
        Log.note("volume = " + volume);
        this.volume = volume;
        return this;
    }
    
    public void write(byte[] bytes) {
        write(bytes, 0, bytes.length);
    }
    
    public void write(byte[] bytes, int offset, int count) {
        synchronized (monitor) {
            var circleBuf = this.circleBuf;
            var insert    = this.insert;
            var length    = circleBuf.length;
            
            if (count > (length - this.writeBuf.length)) {
                throw new IllegalArgumentException("circular buffer too small; count = " + count);
            }
            
            switch (BIT_DEPTH) {
            case 8:
                for (var i = 0; i < count; ++i) {
                    int s = circleBuf[insert] + bytes[offset + i];
                    circleBuf[insert] = (byte) clip8(s);
                    
                    if (( ++insert ) == length) {
                        insert = 0;
                    }
                }
                break;
            case 16:
                for (var i = 0; i < count; i += 2) {
                    int sA = cat(bytes[offset + i], bytes[offset + i + 1]);
                    int sB = cat(circleBuf[insert], circleBuf[insert + 1]);
                    
                    int sum = clip16(sA + sB);
                    pack16(circleBuf, insert, sum);
                    
                    if ((insert += 2) == length) {
                        insert = 0;
                    }
                }
                break;
            default:
                throw new AssertionError(BIT_DEPTH);
            }
        }
    }
    
    private final class PlaybackLoop implements Runnable {
        @Override
        public void run() {
            Log.enter();
            final var line = AudioPlayer.this.line;
            
            if (line == null)
                return;
            
            final var circleBuf = AudioPlayer.this.circleBuf;
            final var circleLen = circleBuf.length;
            final var writeBuf  = AudioPlayer.this.writeBuf;
            final var writeLen  = writeBuf.length;
            final var monitor   = AudioPlayer.this.monitor;
            
            line.start();
            
            var useGainControl = line.isControlSupported(FloatControl.Type.MASTER_GAIN);
            var volume         = -1;
            
            while (isRunning) {
                synchronized (monitor) {
                    var insert = AudioPlayer.this.insert;
                    
                    for (var i = 0; i < writeLen; ++i) {
                        writeBuf[i] = circleBuf[insert];
                        circleBuf[insert] = 0;
                        
                        if (( ++insert ) == circleLen) {
                            insert = 0;
                        }
                    }
                    
                    AudioPlayer.this.insert = insert;
                }
                
                var currentVolume = AudioPlayer.this.volume;
                
                if (useGainControl) {
                    // Note:
                    //  The volume is set this way (as opposed to in the setVolume(int) method),
                    //  because Java sound doesn't seem to document whether Control is thread-safe.
                    //  This just avoids any potential issues that could happen if Control is not
                    //  thread-safe.
                    if (volume != currentVolume) {
                        volume = currentVolume;
                        
                        var ctrl = (FloatControl) line.getControl(FloatControl.Type.MASTER_GAIN);
                        var dB   = (volume == 0) ? ctrl.getMinimum() : (20.0 * Math.log10(volume / 10.0));
                        
                        ctrl.shift(ctrl.getValue(), (float) dB, 500_000);
                    }
                    // Note:
                    //  MASTER_GAIN control uses decibels, and the minimum value
                    //  seems to be about -80dB, which is not actually silent.
                    if (volume == 0) {
                        Arrays.fill(writeBuf, (byte) 0);
                    }
                } else {
                    volume = currentVolume;
                    
                    applyGain(writeBuf, BIT_DEPTH, volume);
                }
                
                var bWritten = line.write(writeBuf, 0, writeLen);
                assert bWritten == writeLen : bWritten;
            }
            
            // TODO: fadeout, maybe
            
            line.close();
        }
    }
    
    // static utilities
    
    private static void applyGain(byte[] bytes, int bitDepth, int volume) {
        int len = bytes.length;
        
        switch (bitDepth) {
        case 8:
            for (var i = 0; i < len; ++i) {
                int s = bytes[i];
                
                s = clip8(s * volume / 10);
                bytes[i] = (byte) s;
            }
            break;
        case 16:
            for (var i = 0; i < len; i += 2) {
                int s = cat(bytes[i], bytes[i + 1]);
                
                s = clip16(s * volume / 10);
                pack16(bytes, i, s);
            }
            break;
        default:
            throw new IllegalArgumentException("bitDepth = " + bitDepth);
        }
    }
    
    private static int getByteCount(AudioFormat fmt, double seconds) {
        var bytes = fmt.getSampleRate() * seconds * fmt.getChannels() * (fmt.getSampleSizeInBits() / Byte.SIZE);
        return (int) Math.floor(bytes);
    }
    
    static int cat(byte b0, byte b1) {
        return (((b0 & 0xFF) << 24) >> 16) | (b1 & 0xFF);
    }
    
    static void pack16(byte[] bytes, int i, int sample) {
        bytes[i] = (byte) (sample >>> 8);
        bytes[i + 1] = (byte) sample;
    }
    
    static int clip8(int sample) {
        return clip(sample, Byte.MIN_VALUE, Byte.MAX_VALUE);
    }
    
    static int clip16(int sample) {
        return clip(sample, Short.MIN_VALUE, Short.MAX_VALUE);
    }
    
    static int clip(int sample, int min, int max) {
        if (sample < min)
            return min;
        if (sample > max)
            return max;
        return sample;
    }
    
    static int getSampleCount(double seconds) {
        return (int) Math.ceil(seconds * SAMPLE_RATE);
    }
    
    static byte[] toBytes(double[] samples) {
        return toBytes(samples, BIT_DEPTH);
    }
    
    static byte[] toBytes(double[] samples, int bitDepth) {
        double fullScale   = (1 << (bitDepth - 2)) - 2;
        int    sampleCount = samples.length;
        byte[] bytes       = new byte[sampleCount * (bitDepth / 8)];
        Random random      = new Random();
        
        for (int i = 0; i < sampleCount; ++i) {
            int sample = (int) Math.floor(fullScale * samples[i] + random.nextInt(2));
            
            switch (bitDepth) {
            case 8:
                bytes[i] = (byte) clip8(sample);
                break;
            case 16:
                pack16(bytes, 2 * i, clip16(sample));
                break;
            default:
                throw new IllegalArgumentException("bitDepth = " + bitDepth);
            }
        }
        
        return bytes;
    }
}