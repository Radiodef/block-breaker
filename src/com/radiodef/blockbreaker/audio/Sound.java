package com.radiodef.blockbreaker.audio;

import java.util.*;

/**
 * @author David Staver
 */
public interface Sound {
    Sound play(AudioPlayer player);
    
    final class FromBytes implements Sound {
        private final byte[] bytes;
        
        public FromBytes(byte[] bytes) {
            this.bytes = Objects.requireNonNull(bytes, "bytes").clone();
        }
        
        @Override
        public Sound play(AudioPlayer player) {
            player.write(bytes);
            return this;
        }
    }
    
    static Sound create(byte[] bytes) {
        return new FromBytes(bytes);
    }
    
    final class OfRandom implements Sound {
        private final Sound[] sounds;
        private final Random  random = new Random();
        private       int     last   = 0;
        
        public OfRandom(Sound... sounds) {
            for (Sound sound : Objects.requireNonNull(sounds, "sounds")) {
                Objects.requireNonNull(sound, "sound");
            }
            this.sounds = sounds.clone();
        }
        
        @Override
        public synchronized Sound play(AudioPlayer player) {
            var sounds = this.sounds;
            var count  = sounds.length;
            var last   = this.last;
            switch (count) {
            case 0:
                return this;
            case 1:
                sounds[0].play(player);
                return this;
            default:
                var index = random.nextInt(count - 1);
                if (index >= last)
                    ++index;
                assert index != last;
                sounds[index].play(player);
                this.last = index;
                return this;
            }
        }
    }
    
    static Sound createRandom(Sound... sounds) {
        return new OfRandom(sounds);
    }
}
