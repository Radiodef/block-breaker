package com.radiodef.blockbreaker.audio;

import java.util.*;

/**
 * @author David Staver
 */
public final class Sequence {
    private static final class Part {
        private final double delayInSeconds;
        private final double[] samples;
        
        private Part(double delayInSeconds, double[] samples) {
            this.delayInSeconds = delayInSeconds;
            this.samples = samples.clone();
        }
    }
    
    private final List<Part> parts = new ArrayList<>();
    
    public Sequence() {
    }
    
    public Sequence add(double delayInSeconds, double[] samples) {
        parts.add(new Part(delayInSeconds, samples));
        return this;
    }
    
    public Sound getSound() {
        return Sound.create(getBytes());
    }
    
    public byte[] getBytes() {
        return AudioPlayer.toBytes(getSamples());
    }
    
    public double[] getSamples() {
        int totalDelay   = 0;
        int totalSamples = 0;
        
        for (Part p : parts) {
            totalDelay  += AudioPlayer.getSampleCount(p.delayInSeconds);
            totalSamples = Math.max(totalSamples, (totalDelay + p.samples.length));
        }
        
        double[] samples = new double[totalSamples];
        int      offset  = 0;
        
        for (Part p : parts) {
            offset += AudioPlayer.getSampleCount(p.delayInSeconds);
            
            for (int i = 0; i < p.samples.length; ++i) {
                samples[offset + i] += p.samples[i];
            }
        }
        
        return samples;
    }
}
