package com.radiodef.blockbreaker.audio;

import com.radiodef.blockbreaker.app.*;

import java.util.*;

/**
 * @author David Staver
 */
public final class Synth {
    public enum Kind { SIN, SAW }
    
    private Kind kind = Kind.SIN;
    
    private double amplitude = 1.0;
    private double fade = 0.0;
    
    private final List<Double> frequencies = new ArrayList<>();
    
    public Synth() {
    }
    
    public Kind getKind() {
        return kind;
    }
    
    public Synth setKind(Kind kind) {
        this.kind = Objects.requireNonNull(kind, "kind");
        return this;
    }
    
    public Synth setAmplitude(double amplitude) {
        this.amplitude = amplitude;
        return this;
    }
    
    public Synth setFrequency(double frequency) {
        frequencies.clear();
        return addFrequency(frequency);
    }
    
    public Synth addFrequency(double frequency) {
        frequencies.add(frequency);
        Log.note("frequencies = " + frequencies);
        return this;
    }
    
    public Synth setNote(String note) {
        return setFrequency(parseFrequency(note));
    }
    
    public Synth addNote(String note) {
        return addFrequency(parseFrequency(note));
    }
    
    public Synth setNotes(String... notes) {
        frequencies.clear();
        for (String note : notes)
            addNote(note);
        return this;
    }
    
    static double parseFrequency(String note) {
        if (!note.matches("[A-G][#b]?[0-9]?")) {
            throw new IllegalArgumentException(note);
        }
        
        int interval;
        
        switch (note.charAt(0)) {
            case 'A':
                interval = 0;
                break;
            case 'B':
                interval = 2;
                break;
            case 'C':
                interval = 3;
                break;
            case 'D':
                interval = 5;
                break;
            case 'E':
                interval = 7;
                break;
            case 'F':
                interval = 8;
                break;
            case 'G':
                interval = 10;
                break;
            default:
                throw new AssertionError(note);
        }
        
        int octave = 4;
        
        if (note.length() > 1) {
            switch (note.charAt(1)) {
                case '#':
                    ++interval;
                    break;
                case 'b':
                    --interval;
                    break;
            }
            
            char last = note.charAt(note.length() - 1);
            
            if ('0' <= last && last <= '9') {
                octave = last - '0';
            }
        }
        
        while (octave > 0) {
            interval += 12;
            --octave;
        }
        
        double f = 27.5 * Math.pow(2, interval / 12.0);
        return f;
    }
    
    public Synth setFade(double fade) {
        if (fade < 0) {
            throw new IllegalArgumentException("fade = " + fade);
        }
        this.fade = fade;
        return this;
    }
    
    public Sound getSound(double lengthInSeconds) {
        return Sound.create(getBytes(lengthInSeconds));
    }
    
    public byte[] getBytes(double lengthInSeconds) {
        return AudioPlayer.toBytes(getSamples(lengthInSeconds));
    }
    
    public double[] getSamples(double lengthInSeconds) {
        int sampleCount = AudioPlayer.getSampleCount(lengthInSeconds);
        if (sampleCount < 0) {
            throw new IllegalArgumentException("lengthInSeconds = " + lengthInSeconds);
        }
        if (sampleCount == 0) {
            return new double[0];
        }
        
        double[]     samples     = new double[sampleCount];
        List<Double> frequencies = this.frequencies;
        double       amplitude   = this.amplitude / Math.max(1, frequencies.size());
        
        switch (kind) {
        case SIN:
            fill(samples, frequencies, amplitude, Synth::computeSin);
            break;
        case SAW:
            fill(samples, frequencies, amplitude, Synth::computeSaw);
            break;
        default:
            throw new AssertionError(kind);
        }
        
        if (this.fade != 0.0) {
            int fadeCount = Math.min(sampleCount, AudioPlayer.getSampleCount(this.fade));
            int fadeStart = 0;
            
            if (fadeCount < sampleCount) {
                fadeStart = (sampleCount - fadeCount);
            }
            
            for (int i = 0; i < fadeCount; ++i) {
                double scale = (double) (fadeCount - i) / (double) fadeCount;
                samples[fadeStart + i] *= scale;
            }
        }
        
        return samples;
    }
    
    private static void fill(double[] samples, List<Double> frequencies, double amplitude, SampleFunction fn) {
        int sampleCount    = samples.length;
        int frequencyCount = frequencies.size();
        
        for (int i = 0; i < sampleCount; ++i) {
            double s = 0.0;
            
            for (var j = 0; j < frequencyCount; ++j) {
                s += amplitude * fn.compute(i, frequencies.get(j));
            }
            
            samples[i] = s;
        }
    }
    
    @FunctionalInterface
    private interface SampleFunction {
        double compute(int i, double f);
    }
    
    private static double computeSin(int i, double f) {
        double w = 2.0 * Math.PI * f;
        double t = (double) i / (double) AudioPlayer.SAMPLE_RATE;
        return Math.sin(w * t);
    }
    
    private static double computeSaw(int i, double f) {
        double t = (double) i / (double) AudioPlayer.SAMPLE_RATE;
        double p = 1.0 / f;
        double r = t / p;
        return (2.0 * (r - Math.floor(r))) - 1.0;
    }
    
    @Override
    public String toString() {
        return String.format("Synth{kind = %s, amplitude = %f, fade = %f, frequencies = %s}",
                             kind, amplitude, fade, frequencies);
    }
}