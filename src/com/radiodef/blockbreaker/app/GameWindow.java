package com.radiodef.blockbreaker.app;

import com.radiodef.blockbreaker.main.*;
import com.radiodef.blockbreaker.util.*;
import com.radiodef.blockbreaker.game.*;

import java.awt.*;
import javax.swing.*;

/**
 * @author David Staver
 */
public final class GameWindow {
    private final JFrame frame;

    private final GamePanel panel;

    private Game game;

    public GameWindow() {
        Log.enter();
        Tools.requireEDT();

        frame = new JFrame(BlockBreaker.NAME);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        panel = new GamePanel();
        frame.setContentPane(panel);

        frame.pack();
        fitSize();
        frame.setLocationRelativeTo(null);
    }

    public void setGame(Game game) {
        Tools.requireEDT();
        if (game != null && game.getWindow() != this) {
            throw new IllegalArgumentException(game.toString());
        }

        if (this.game != null) {
            this.game.close();
        }

        this.game = game;
        panel.setGame(game);

        if (game != null) {
            Tools.whenShowing(frame, game::startLoop);
        }
    }

    public JFrame getFrame() {
        Tools.requireEDT();
        return frame;
    }

    public GamePanel getPanel() {
        Tools.requireEDT();
        return panel;
    }

    private void fitSize() {
        Dimension screen = Tools.getAvailableScreenSize(frame);
        Dimension aspect = Scene.getDefaultAspectRatio();
        Insets    insets = frame.getInsets();

        int insX = insets.left + insets.right;
        int insY = insets.top  + insets.bottom;

        int height, width;

        if (screen.height < screen.width) {
            // width = height * W / H
            height = screen.height / 2;
            width  = ((height - insY) * aspect.width / aspect.height) + insX;
        } else {
            // height = width * H / W
            width  = screen.width / 2;
            height = ((width - insX) * aspect.height / aspect.width) + insY;
        }

        Log.notef("width = %d, height = %d", width, height);
        frame.setSize(width, height);
    }
}
