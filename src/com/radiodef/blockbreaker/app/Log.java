package com.radiodef.blockbreaker.app;

import com.radiodef.blockbreaker.util.*;

import static java.lang.System.out;

/**
 * @author David Staver
 */
public final class Log {
    private Log() {
    }

    public static void enter() {
        note("entered", 1);
    }

    public static void note(Object message) {
        note(message, 1);
    }

    public static void notef(String fmt, Object... args) {
        note(String.format(fmt, args), 1);
    }

    private static void note(Object message, int skip) {
        var caller    = Tools.getCaller(skip + 1);
        var className = caller.getClassName();
        var dot       = className.lastIndexOf('.');
        if (dot >= 0)
            className = className.substring(dot + 1);
        className = className.replace('$', '.');
        out.print(className);
        out.print('.');
        out.print(caller.getMethodName());
        out.print(": ");
        out.println(message);
    }

    public static void caught(Throwable x) {
        note("caught " + x, 1);
        x.printStackTrace(out);
    }
}
