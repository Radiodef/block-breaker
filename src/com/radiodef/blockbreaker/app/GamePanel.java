package com.radiodef.blockbreaker.app;

import com.radiodef.blockbreaker.util.*;
import com.radiodef.blockbreaker.game.*;

import java.awt.*;
import javax.swing.*;

/**
 * @author David Staver
 */
public final class GamePanel extends JPanel {
    private Game game;

    GamePanel() {
        Tools.requireEDT();

        setLayout(null);
        setBackground(Color.LIGHT_GRAY);
    }

    void setGame(Game game) {
        Tools.requireEDT();

        this.game = game;
        repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        Game game = this.game;

        if (game != null) {
            try (GraphicsRef g2 = new GraphicsRef(g)) {
                game.getScene().draw(game, g2.get());
            }
        }
    }
}
