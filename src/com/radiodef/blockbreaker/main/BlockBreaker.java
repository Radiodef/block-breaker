package com.radiodef.blockbreaker.main;

import com.radiodef.blockbreaker.app.*;
import com.radiodef.blockbreaker.game.*;

import javax.swing.*;

import java.util.*;

/**
 * @author David Staver
 */
public final class BlockBreaker {
    private BlockBreaker() {
    }
    
    public static final String NAME = "Block Breaker";
    
    private static volatile Set<String> ARGS = Collections.emptySet();
    
    public static boolean isDebug() {
        return ARGS.contains("debug");
    }
    
    public static boolean isMac() {
        return System.getProperty("os.name").startsWith("Mac OS");
    }
    
    public static void main(String[] args) {
        Log.enter();
        
        Log.note("args = " + Arrays.toString(args));
        var argsSet = new LinkedHashSet<String>();
        Collections.addAll(argsSet, args);
        ARGS = Collections.unmodifiableSet(argsSet);
        
        if (isMac()) {
            System.setProperty("com.apple.mrj.application.apple.menu.about.name", NAME);
            System.setProperty("apple.awt.application.name", NAME);
        }
        
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception x) {
            Log.caught(x);
        }
        
        SwingUtilities.invokeLater(() -> {
            var game = new Game();
            game.getWindow().getFrame().setVisible(true);
        });
    }
}